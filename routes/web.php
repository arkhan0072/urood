<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@wellcome')->name('default');
//Route::get('/Dashboard','UserController@dashboard')->name('userDashboard');
Route::get('/Dashboard','HomeController@index')->name('dashboard');
Route::get('/order','OrderController@index')->name('orderD');
Route::get('/profile','UserController@userprofile')->name('profile');
Route::get('/changepassword','UserController@changepassword')->name('changepassword');
Route::post('/updateProfile/{id}','UserController@updateProfile')->name('Updateprofile');
Route::post('/updatePassword/{id}','UserController@updatePassword')->name('updatePassword');
Route::resource('users', 'UserController');
Route::resource('countries', 'CountryController');
Route::resource('hotels', 'HotelController');
Route::resource('photos', 'PhotoController');
Route::resource('airports', 'AirPortController');
Route::resource('advertisments', 'AdvertismentController');
Route::resource('pages', 'PageController');
Route::get('/getPackage/{id}', 'PageController@getPackage')->name('getPackage');
Route::resource('packages', 'PackageController');
Route::resource('orders', 'OrderController');
Route::get('ajax/user','UserController@ajax')->name('ajaxUser');
Route::get('ajax/country','CountryController@ajax')->name('ajaxCountry');
Route::get('ajax/hotel','HotelController@ajax')->name('ajaxHotel');
Route::get('ajax/airport','AirPortController@ajax')->name('ajaxAirport');
Route::get('ajax/advertisement','AdvertismentController@ajax')->name('ajaxAdvertisement');
Route::get('ajax/pages','PageController@ajax')->name('ajaxPage');
Route::get('ajax/package','PackageController@ajax')->name('ajaxPackage');
Route::get('ajax/order','OrderController@ajax')->name('orderAjax');
Auth::routes();
Route::get('/registeragency','PageController@agency')->name('regAgecny');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/activate/token={token}', 'ActivationController@activate')->name('activate');
