<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

    public function photos(){
       return  $this->hasMany('App\Photo');
    }
    public function hotelsmakha(){
        return  $this->belongsTo('App\Hotel','makkahhotel_id');
    }
    public function madinahhotel(){
        return  $this->belongsTo('App\Hotel','madinahhotel_id');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
    public function features(){
        return $this->hasMany('App\Feature');
    }
    public function price(){
        return $this->hasMany('App\Price');
    }
    public function tickets(){
        return $this->hasMany('App\Ticket');
    }
}
