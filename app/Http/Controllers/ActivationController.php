<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ActivationController extends Controller
{
    public function activate(Request $request){
        $token=$request->token;
//        return $token;
        $user=User::where('activation_code',$token)->first();
//        return $user;
        if($user){
        if($token==$user->activation_code){
            $user->email_verified=1;
            $user->save();
            return view('activate',compact('user'));
        }else{
            return view('auth.login');
        }
    }else{
        return view('auth.login');
        }
    }
}
