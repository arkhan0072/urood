<?php

namespace App\Http\Controllers;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('countries.index');
        }else{
            return redirect()->back();
        }
    }
    public function ajax(){
        $countries=Country::where('id','>',0);
        return Datatables::of($countries)
            ->addColumn('action', function(Country $country){
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="'.route("countries.edit", $country->id).'"><i class="fa fa-edit"></i></a>';

                return $btn.view(' countries.delete', compact('country'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->user_level==4) {
        return view('countries.create');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'iso' => ['required', 'string', 'max:50'],
            'iso_3' => ['required', 'string', 'max:50'],
            'name' => ['required', 'string', 'max:50'],
            'nice_name' => ['required', 'string', 'max:50'],
            'numcode' => ['required', 'string','max:50'],
            'phonecode' => ['required', 'string','max:50'],
        ]);
        $country = new Country();
        $country->iso = $request->iso;
        $country->iso_3 = $request->iso_3;
        $country->name = $request->name;
        $country->nice_name = $request->nice_name;
        $country->numcode = $request->numcode;
        $country->phonecode = $request->phonecode;
        $country->save();
        return redirect(route('countries.index'))->with('msg', 'Country created Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('countries.edit', compact('country'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        $request->validate([
            'iso' => ['required', 'string', 'max:50'],
            'iso_3' => ['required', 'string', 'max:50'],
            'name' => ['required', 'string', 'max:50'],
            'nice_name' => ['required', 'string', 'max:50'],
            'numcode' => ['required', 'string','max:50'],
            'phonecode' => ['required', 'string','max:50'],
        ]);
        $country->iso = $request->iso;
        $country->iso_3 = $request->iso_3;
        $country->name = $request->name;
        $country->nice_name = $request->nice_name;
        $country->numcode = $request->numcode;
        $country->phonecode = $request->phonecode;
        $country->save();
        return redirect(route('countries.index'))->with('msg', 'Country Updated Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        $country->delete();
        return redirect(route('countries.index'))->with('msg', 'Country deleted Successfuly');

    }
}
