<?php

namespace App\Http\Controllers\Auth;

use App\Country;
use App\Events\SendEmail;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string','max:50'],
            'state' => ['required', 'string','max:50'],
            'zipcode' => ['required', 'string','max:20'],
            'country' => ['required', 'string'],
            'homenumber' => ['required', 'string','max:20'],
            'mobilenumber' => ['required', 'string','max:20'],
            'user_level' => ['required', 'string','max:20']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'fname' => $data['fname'] ,
            'lname' => $data['lname'] ,
            'email' => $data['email'] ,
            'password' =>  Hash::make($data['password']) ,
            'address' => $data['address'] ,
            'city' => $data['city'] ,
            'state' => $data['state'] ,
            'zipcode' => $data['zipcode'] ,
            'country_id' => $data['country'] ,
            'homephone' => $data['homenumber'] ,
            'mobilephone' => $data['mobilenumber'],
            'user_level' => $data['user_level']
        ]);
        // if($data['role']){
        //    $user = $user->assignRole($data['role']);
        // }else{
        //     $user = $user->assignRole(1);
        // }
//
//        $user->assignRole('user');
        event(new Registered($user));
        event(new SendEmail($user));
         return $user;

    }


}
