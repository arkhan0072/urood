<?php

namespace App\Http\Controllers;

use App\Country;
use App\Package;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('pages.index');
        }else{
            return redirect()->back();
        }
    }
    public function ajax(){
        $pages=Page::where('id','>',0);
        return Datatables::of($pages)
            ->addColumn('action', function(Page $page){
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="'.route("pages.edit", $page->id).'"><i class="fa fa-edit"></i></a>';

                return $btn.view(' pages.delete', compact('page'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('pages.create');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'slug' => ['required', 'string','max:255'],
            'content' => ['required', 'string'],
        ]);
        $page= new Page();
        $page->title = $request->title;
        $page->slug = $request->title;
        $page->content = $request->details;
        $page->showmenu = $request->showmenu;
        $page->save();
        return redirect(route('pages.index'))->with('msg', 'Page created Successfuly');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('pages.edit', compact('page'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'slug' => ['required', 'string','max:255'],
            'content' => ['required', 'string'],
        ]);
        $page->title = $request->title;
        $page->slug = $request->title;
        $page->content = $request->details;
        $page->showmenu = $request->showmenu;
        $page->save();
        return redirect(route('pages.index'))->with('msg', 'Page Updated Successfuly');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
       $page->delete();
        return redirect(route('pages.index'))->with('msg', 'Page Deleted Successfuly');

    }
    public function agency(){
        $countries=Country::all();
        return view('auth.agencyRegistration',compact('countries'));
    }
    public function getPackage(Request $request)
    {
        $package=Package::where('id',$request->id)->first();
        $package->load('hotelsmakha','madinahhotel','photos','price','tickets');
//        return $package;
        return view('packages.details',compact('package'));
    }
}
