<?php

namespace App\Http\Controllers;

use App\AirPort;
use App\Country;
use App\Feature;
use App\Hotel;
use App\Package;
use App\Photo;
use App\Price;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['packages\{id}']]);

    }

    public function index()
    {
        $user=Auth::user();
        if($user->user_level==2 || $user->user_level==3 || $user->user_level==4 ) {
            return view('packages.index');
        }else{
            return redirect()->back();
        }
    }
    public function ajax(){
        $user=Auth::user();
        if($user->user_level==2||$user->user_level==3) {
            $packages = Package::where('id', '>', 0)->where('user_id', $user->id);
        }else{
            $packages = Package::where('id', '>', 0);
        }
//        $packages->with('countries','photos','price','features');
        $packages->with('country','hotelsmakha','madinahhotel');
        return Datatables::of($packages)
            ->addColumn('action', function(Package $package){
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="'.route("packages.edit", $package->id).'"><i class="fa fa-edit"></i></a>';
                return $btn.view('packages.delete', compact('package'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user=Auth::user();
        if($user->user_level==2 || $user->user_level==3 || $user->user_level==4 ) {
            $startAirports = AirPort::where('airport_type', 1)->get();
            $endAirports = AirPort::where('airport_type', 2)->get();
            $MadinahHotels = Hotel::where('city', 'Madinah')->get();
            $MakkahHotels = Hotel::where('city', 'Makkah')->get();
            $countries = Country::all();
            return view('packages.create', compact('startAirports', 'endAirports', 'MadinahHotels', 'MakkahHotels', 'countries'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'packagetype' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'agency_id' => ['required', 'string', 'max:255'],
            'numberofpackages' => ['required', 'string', 'max:255'],
            'depart_date' => ['required', 'string', 'max:255'],
            'return_date' => ['required', 'string', 'max:255'],
            'totaldays' => ['required', 'string', 'max:255'],
            'itineray' => ['required', 'string', 'max:255'],
            'makkahhotel_id' => ['required', 'string', 'max:255'],
            'madinahhotel_id' => ['required', 'string', 'max:255'],
            'startingcity' => ['required', 'string', 'max:255'],
            'country_id' => ['required', 'string', 'max:255'],
            'side_trip_start_date' => ['required', 'string', 'max:255'],
            'side_trip_end_date' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255'],
            'appearsabovesearch' => [ 'string', 'max:255'],
            'appearsbelowsearchbox' => ['string', 'max:255'],
            'air_condition_bus' => ['string', 'max:255'],
            'buffet_breakfast_during_hotel_stay_in_madinah' => [ 'string', 'max:255'],
            'buffet_breakfast_during_hotel_stay_in_makkah' => ['string', 'max:255'],
            'food_in_mina' => ['string', 'max:255'],
            'food_in_arafat' => ['string', 'max:255'],
            'no_food_included_in_the_package' => ['string', 'max:255'],
            'regular_american_tents' => ['string', 'max:255'],
            'regular_air_conditioned_in_mina' => ['string', 'max:255'],
            'camp_in_front_of_the_jamarat_mena' => ['string', 'max:255'],
            'educational_program_english' => ['string', 'max:255'],
            'educational_program_arabic' => ['string', 'max:255'],
            'educational_program_urdu' => ['string', 'max:255'],
        ]);
//        return $request->file('pic');
        $user=Auth::user();
        if( $user->user_level==3 || $user->user_level==4 ) {
        $purchase_type=1;
        }else{
            $purchase_type=0;
        }
        $package= new Package();
        $package->packagetype = $request->packagetype;
        $package->title = $request->title;
        $package->description = $request->description;
        $package->agency_id = $request->agency;
        $package->numberofpackages = $request->numberofpackages;
        $package->depart_date = $request->depart_date;
        $package->return_date = $request->return_date;
        $package->totaldays = $request->totaldays;
        $package->itineray = $request->itineray;
        $package->makkahhotel_id = $request->makkahhotel_id;
        $package->madinahhotel_id = $request->madinahhotel_id;
        $package->startingcity = $request->startingcity;
        $package->country_id = $request->country_id;
        $package->side_trip_start_date = $request->side_trip_start_date;
        $package->side_trip_end_date = $request->side_trip_end_date;
        $package->status = $request->status;
        $package->appearsabovesearch = $request->appearsabovesearch;
        $package->appearsbelowsearchbox = $request->appearsbelowsearchbox;
        $package->air_condition_bus = $request->air_condition_bus;
        $package->buffet_breakfast_during_hotel_stay_in_madinah = $request->buffet_breakfast_during_hotel_stay_in_madinah;
        $package->buffet_breakfast_during_hotel_stay_in_makkah = $request->buffet_breakfast_during_hotel_stay_in_makkah;
        $package->food_in_mina = $request->food_in_mina;
        $package->food_in_arafat = $request->food_in_arafat;
        $package->no_food_included_in_the_package = $request->no_food_included_in_the_package;
        $package->regular_american_tents = $request->regular_american_tents;
        $package->regular_air_conditioned_in_mina = $request->regular_air_conditioned_in_mina;
        $package->camp_in_front_of_the_jamarat_mena = $request->camp_in_front_of_the_jamarat_mena;
        $package->educational_program_english = $request->educational_program_english;
        $package->educational_program_arabic = $request->educational_program_arabic;
        $package->educational_program_urdu = $request->educational_program_urdu;
        $package->guide_language_english = $request->guide_language_english;
        $package->guide_language_arabic = $request->guide_language_arabic;
        $package->guide_language_urdu = $request->guide_language_urdu;
        $package->literature_english = $request->literature_english;
        $package->literature_arabic = $request->literature_arabic;
        $package->literature_urdu = $request->literature_urdu;
        $package->purchase_type = $purchase_type;
        $package->user_id = $user->id;
        $package->save();
        if($request->file('pic')){
            foreach($request->pic as $picture){
                $pictures = new Photo();
                $pictures->package_id = $package->id;
                $file_extension = $picture->getClientOriginalExtension();
                $file_path = $picture->getFilename();
                $filename = $package->id . '_' . Str::random(8) . '.' . $file_extension;
                $pictures->name = $filename;
                Storage::disk('packages')->put($filename, \File::get($picture));
                $pictures->save();
            }
        }
         if($request->price){
            foreach($request->price as $pricez){
              foreach($request->people_per_room as $people){
                $price = new Price();
                $price->package_id = $package->id;
                $price->price = $pricez;
                $price->people_per_room = $people;
                $price->save();
              }
             }
         }
        foreach($request->name as $featurez){
                    $feature = new Feature();
                $feature->package_id = $package->id;
                $feature->name = $featurez;
                $feature->save();
        }
        foreach($request->startairport_id as $startairport){
            foreach($request->endairport_id as $endairport){
            foreach($request->roundtrip as $roundtrip){
                $ticket = new Ticket();
                $ticket->package_id = $package->id;
                $ticket->startairport_id = $startairport;
                $ticket->endairport_id = $endairport;
                $ticket->roundtrip = $roundtrip;
                $ticket->save();
            }
            }
        }
        return redirect(route('packages.index'))->with('msg', 'Package Added Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param Package $package
     * @return Response
     */
    public function show(Package $package)
    {
        return  $package;
        return view('pacakges.details',compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Package $package
     * @return Response
     */
    public function edit(Package $package)
    {
        $user = Auth::user();
        if ($user->user_level == 2 || $user->user_level == 3 || $user->user_level == 4) {
            $startAirports = AirPort::where('airport_type', 1)->get();
            $endAirports = AirPort::where('airport_type', 2)->get();
            $MadinahHotels = Hotel::where('city', 'Madinah')->get();
            $MakkahHotels = Hotel::where('city', 'Makkah')->get();
            $countries = Country::all();
            return view('packages.edit', compact('startAirports', 'endAirports', 'MadinahHotels', 'MakkahHotels', 'countries', 'package'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Package $package
     * @return Response
     */
    public function update(Request $request, Package $package)
    {
        $request->validate([
            'packagetype' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'agency_id' => ['required', 'string', 'max:255'],
            'numberofpackages' => ['required', 'string', 'max:255'],
            'depart_date' => ['required', 'string', 'max:255'],
            'return_date' => ['required', 'string', 'max:255'],
            'totaldays' => ['required', 'string', 'max:255'],
            'itineray' => ['required', 'string', 'max:255'],
            'makkahhotel_id' => ['required', 'string', 'max:255'],
            'madinahhotel_id' => ['required', 'string', 'max:255'],
            'startingcity' => ['required', 'string', 'max:255'],
            'country_id' => ['required', 'string', 'max:255'],
            'side_trip_start_date' => ['required', 'string', 'max:255'],
            'side_trip_end_date' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255'],
            'appearsabovesearch' => [ 'string', 'max:255'],
            'appearsbelowsearchbox' => ['string', 'max:255'],
            'air_condition_bus' => ['string', 'max:255'],
            'buffet_breakfast_during_hotel_stay_in_madinah' => [ 'string', 'max:255'],
            'buffet_breakfast_during_hotel_stay_in_makkah' => ['string', 'max:255'],
            'food_in_mina' => ['string', 'max:255'],
            'food_in_arafat' => ['string', 'max:255'],
            'no_food_included_in_the_package' => ['string', 'max:255'],
            'regular_american_tents' => ['string', 'max:255'],
            'regular_air_conditioned_in_mina' => ['string', 'max:255'],
            'camp_in_front_of_the_jamarat_mena' => ['string', 'max:255'],
            'educational_program_english' => ['string', 'max:255'],
            'educational_program_arabic' => ['string', 'max:255'],
            'educational_program_urdu' => ['string', 'max:255'],
        ]);
        $user=Auth::user();
        if($user->user_level==3 || $user->user_level==4 ) {
            $purchase_type=1;
        }else{
            $purchase_type=0;
        }
        $package->packagetype = $request->packagetype;
        $package->title = $request->title;
        $package->description = $request->description;
        $package->agency_id = $request->agency;
        $package->numberofpackages = $request->numberofpackages;
        $package->depart_date = $request->depart_date;
        $package->return_date = $request->return_date;
        $package->totaldays = $request->totaldays;
        $package->itineray = $request->itineray;
        $package->makkahhotel_id = $request->makkahhotel_id;
        $package->madinahhotel_id = $request->madinahhotel_id;
        $package->startingcity = $request->startingcity;
        $package->country_id = $request->country_id;
        $package->side_trip_start_date = $request->side_trip_start_date;
        $package->side_trip_end_date = $request->side_trip_end_date;
        $package->status = $request->status;
        $package->appearsabovesearch = $request->appearsabovesearch;
        $package->appearsbelowsearchbox = $request->appearsbelowsearchbox;
        $package->air_condition_bus = $request->air_condition_bus;
        $package->buffet_breakfast_during_hotel_stay_in_madinah = $request->buffet_breakfast_during_hotel_stay_in_madinah;
        $package->buffet_breakfast_during_hotel_stay_in_makkah = $request->buffet_breakfast_during_hotel_stay_in_makkah;
        $package->food_in_mina = $request->food_in_mina;
        $package->food_in_arafat = $request->food_in_arafat;
        $package->no_food_included_in_the_package = $request->no_food_included_in_the_package;
        $package->regular_american_tents = $request->regular_american_tents;
        $package->regular_air_conditioned_in_mina = $request->regular_air_conditioned_in_mina;
        $package->camp_in_front_of_the_jamarat_mena = $request->camp_in_front_of_the_jamarat_mena;
        $package->educational_program_english = $request->educational_program_english;
        $package->educational_program_arabic = $request->educational_program_arabic;
        $package->educational_program_urdu = $request->educational_program_urdu;
        $package->guide_language_english = $request->guide_language_english;
        $package->guide_language_arabic = $request->guide_language_arabic;
        $package->guide_language_urdu = $request->guide_language_urdu;
        $package->literature_english = $request->literature_english;
        $package->literature_arabic = $request->literature_arabic;
        $package->literature_urdu = $request->literature_urdu;
        $package->purchase_type = $purchase_type;
        $package->save();
        if($request->file('pic')){
            foreach($request->pic as $picture){
                $pictures = new Photo();
                $pictures->package_id = $package->id;
                $file_extension = $picture->getClientOriginalExtension();
                $file_path = $picture->getFilename();
                $filename = $package->id . '_' . Str::random(8) . '.' . $file_extension;
                $pictures->name = $filename;
                Storage::disk('packages')->put($filename, \File::get($picture));
                $pictures->save();
            }
        }

        if($request->price){
            foreach($request->price as $pricez){
                foreach($request->people_per_room as $people){
                    $price = new Price();
                    $price->package_id = $package->id;
                    $price->price = $pricez;
                    $price->people_per_room = $people;
                    $price->save();
                }
            }
        }
        foreach($request->name as $featurez){
            $feature = new Feature();
            $feature->package_id = $package->id;
            $feature->name = $featurez;
            $feature->save();
        }
        foreach($request->startairport_id as $startairport){
        foreach($request->endairport_id as $endairport){
            $ticket = new Ticket();
            $ticket->package_id = $package->id;
            $ticket->startairport_id = $startairport->startairport_id;
            $ticket->endairport_id = $endairport->endairport_id;
            $ticket->roundtrip = $endairport->roundtrip;
            $ticket->save();
        }
        }
        return redirect(route('packages.index'))->with('msg', 'Package Updated Successfuly');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Package $package
     * @return Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return redirect(route('packages.index'))->with('msg', 'Package deleted Successfully');
    }

}
