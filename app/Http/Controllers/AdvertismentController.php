<?php

namespace App\Http\Controllers;

use App\Advertisment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class AdvertismentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('advertisments.index');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('advertisments.create');
        }else{
            return redirect()->back();
        }
    }
    public function ajax(){
        $advertisment=Advertisment::where('id','>',0);
        return Datatables::of($advertisment)
            ->addColumn('action', function(Advertisment $advertisment){
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="'.route("advertisments.edit", $advertisment->id).'"><i class="fa fa-edit"></i></a>';

                return $btn.view(' advertisments.delete', compact('advertisment'))->render();
            })
                ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $advertisment = new Advertisment();
       $advertisment->location = $request->location;
       $advertisment->content = $request->details;
       $advertisment->status = $request->status;
       $advertisment->homePage = $request->homePage;
       $advertisment->save();
        return redirect(route('advertisments.index'))->with('msg', 'Advertisment created Successfuly');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertisment  $advertisment
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisment $advertisment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertisment  $advertisment
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisment $advertisment)
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('advertisments.edit', compact('advertisment'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertisment  $advertisment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisment $advertisment)
    {
        $advertisment->location = $request->location;
        $advertisment->content = $request->details;
        $advertisment->status = $request->status;
        $advertisment->homePage = $request->homePage;
        $advertisment->save();
        return redirect(route('advertisments.index'))->with('msg', 'Advertisment Updated Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertisment  $advertisment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisment $advertisment)
    {
        $advertisment->delete();
        return redirect(route('advertisments.index'))->with('msg', 'Advertisment deleted Successfuly');

    }
}
