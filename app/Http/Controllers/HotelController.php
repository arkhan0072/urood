<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        $this->middleware('auth');
    }
    public function index()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('hotels.index');
        }else{
            return redirect()->back();
        }
    }
    public function ajax(){
        $hotels=Hotel::where('id','>',0);
        return Datatables::of($hotels)
            ->addColumn('action', function(Hotel $hotel){
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="'.route("hotels.edit", $hotel->id).'"><i class="fa fa-edit"></i></a>';

                return $btn.view(' hotels.delete', compact('hotel'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->user_level==4) {
            return view('hotels.create');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string','max:50'],
            'star' => ['required', 'string','max:50'],
            'description' => ['required', 'string'],
        ]);
        $hotel  = new Hotel();
       $hotel->name = $request->name ;
       $hotel->city = $request->city ;
       $hotel->star = $request->star ;
       $hotel->description = $request->description ;
       $hotel->save() ;
       if($request->file('pic')){
         $file = $request->file('image');
         foreach($request->pic as $picture){
                $pictures = new Photo();
                 $pictures->hotel_id = $hotel->id;
                 $file_extension = $picture->getClientOriginalExtension();
                 $file_path = $picture->getFilename();
                $filename = $hotel->id . '_' . Str::random(8) . '.' . $file_extension;
                 $pictures->name = $filename;
            Storage::disk('hotels')->put($filename, \File::get($picture));
            $pictures->save();
          }
       }
        return redirect(route('hotels.index'))->with('msg', 'Hotel created Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        $user = Auth::user();
        if($user->user_level==4) {
            $pictures = Photo::where('hotel_id', $hotel->id)->get();
            return view('hotels.edit', compact('hotel', 'pictures'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string','max:50'],
            'star' => ['required', 'string','max:50'],
            'description' => ['required', 'string'],
        ]);
       $hotel->name = $request->name ;
       $hotel->city = $request->city ;
       $hotel->star = $request->star ;
       $hotel->description = $request->description ;
       $hotel->save() ;
       if($request->file('pic')){
         $file = $request->file('image');
         foreach($request->pic as $picture){
                 $pictures->hotel_id = $hotel->id;
                 $file_extension = $picture->getClientOriginalExtension();
                 $file_path = $picture->getFilename();
                $filename = $hotel->id . '_' . str_random(8) . '.' . $file_extension;
                 $pictures->name = $filename;
            Storage::disk('hotels')->put($filename, \File::get($picture));
            $pictures->save();
          }
       }
        return redirect(route('hotels.index'))->with('msg', 'Hotel updated Successfuly');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();
        return redirect(route('hotels.index'))->with('msg', 'Hotel deleted Successfuly');

    }
}
