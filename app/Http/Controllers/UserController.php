<?php

namespace App\Http\Controllers;

use App\Country;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->user_level == 4) {
            return view('users.index');
        } else {
            return redirect()->back();
        }
    }

    public function ajax()
    {
        $users = User::where('id', '>', 0);
        $users->with('country');
        return Datatables::of($users)
            ->addColumn('action', function (User $user) {
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="' . route("users.edit", $user->id) . '"><i class="fa fa-edit"></i></a>';
                return $btn . view(' users.delete', compact('user'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->user_level == 4) {
            $countries = Country::all();
            return view('users.add', compact('countries'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string','max:50'],
            'state' => ['required', 'string','max:50'],
            'zipcode' => ['required', 'string','max:20'],
            'country' => ['required', 'string'],
            'homenumber' => ['required', 'string','max:20'],
            'mobilenumber' => ['required', 'string','max:20'],
            'officephone' => ['string','max:20'],
            'user_level' => ['required', 'string','max:20']
        ]);
        $user = new User();
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zipcode = $request->zipcode;
        $user->country_id = $request->country;
        $user->homephone = $request->homenumber;
        $user->mobilephone = $request->mobilenumber;
        $user->officephone = $request->officephone;
        $user->password = Hash::make($request->password);
        $user->user_level = $request->user_level;
        $user->save();
        return redirect(route('users.index'))->with('msg', 'User Created Successfuly');

    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function edit(User $user)
    {
//        return $user;
        $auth = Auth::user();
        $countries=Country::all();
        if ($auth->user_level == 4) {
            return view('users.edit', compact('user','countries'));
        }else {
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'address' => ['required', 'string'],
            'city' => ['required', 'string','max:50'],
            'state' => ['required', 'string','max:50'],
            'zipcode' => ['required', 'string','max:20'],
            'country' => ['required', 'string'],
            'homenumber' => ['required', 'string','max:20'],
            'mobilenumber' => ['required', 'string','max:20'],
            'officephone' => ['string','max:20'],
            'user_level' => ['required', 'string','max:20']
        ]);
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zipcode = $request->zipcode;
        $user->country_id = $request->country;
        $user->approved_status = $request->status_approved;
        $user->approved_by=Auth::id();
        $user->homephone = $request->homenumber;
        $user->mobilephone = $request->mobilenumber;
        $user->officephone = $request->officephone;
        $user->user_level = $request->user_level;
        $user->save();
        return redirect(route('users.index'))->with('msg', 'User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect(route('users.index'))->with('msg', 'User deleted Successfuly');
    }

    public function dashboard()
    {
        $user = Auth::user();
        if ($user->user_level == 1) {
            return view('userdashboard', compact('user'));
        } else if ($user->user_level == 2) {
            return view('regulardashboard', compact('user'));
        } elseif ($user->user_level == 3) {
            return view('partnerdashboard', compact('user'));
        } elseif ($user->user_level == 4) {
            return view('home', compact('user'));
        } else {
            return view('userdashboard');
        }
    }
    public function userprofile(){
        $user=Auth::user();
        return view('users.profile',compact('user'));
    }
    public function changepassword(){
        $user=Auth::user();
        return view('changepass',compact('user'));
    }
    public function updatePassword(Request $request,User $user){
            $user=Auth::user();
            $user->password = Hash::make($request->password);
            $user->save();
        return redirect()->back();
    }
}
