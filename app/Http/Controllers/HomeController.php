<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => 'wellcome']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user=Auth::user();
        if ($user->user_level==1) {
            return view('userdashboard');
        }else if($user->user_level==2) {
            return view('regulardashboard');
        }elseif($user->user_level==3) {
            return view('partnerdashboard');
        }elseif($user->user_level==4) {
            return view('home');
        }else{
            return view('userdashboard');
        }

    }
    public function wellcome(){
        $premium_packages=Package::where('purchase_type',1)->get();
        $premium_packages->load('price','photos');
//        return $premium_packages;
        $regular_packages=Package::where('purchase_type',0)->get();
        $regular_packages->load('price','photos');
        return view('welcome',compact('premium_packages','regular_packages'));
    }

}
