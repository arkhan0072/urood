<?php

namespace App\Http\Controllers;

use App\AirPort;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class AirPortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user=Auth::user();
        if($user->user_level==2 ||$user->user_level==3 ||$user->user_level==4) {
            return view('airports.index');
        }else{
            return redirect()->back();
        }
    }
    public function ajax(){
        $airports=AirPort::where('id','>',0);
        return Datatables::of($airports)
            ->addColumn('action', function(AirPort $airport){
                $btn = '<a href=""><i class="fa fa-eye"></i></a>
	   	<a href="'.route("airports.edit", $airport->id).'"><i class="fa fa-edit"></i></a>';
        return $btn.view('airports.delete', compact('airport'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        if($user->user_level==4) {
        return view ('airports.create');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'airport_type' => ['required', 'string', 'max:50'],
            'location' => ['required', 'string', 'max:50'],
        ]);
        $user=Auth::user();
        if($user->user_level==4) {
            $airport = new AirPort();
            $airport->title = $request->title;
            $airport->airport_type = $request->airport_type;
            $airport->location = $request->location;
            $airport->save();
            return redirect(route('airports.index'))->with('msg', 'AirPort Added Successfuly');
        }else{
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AirPort  $airPort
     * @return \Illuminate\Http\Response
     */
    public function show(AirPort $airPort)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AirPort  $airPort
     * @return \Illuminate\Http\Response
     */
    public function edit(AirPort $airport)
    {
        $user=Auth::user();
        if($user->user_level==4) {
            return view('airports.edit', compact('airport'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AirPort  $airPort
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AirPort $airport)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'airport_type' => ['required', 'string', 'max:50'],
            'location' => ['required', 'string', 'max:50'],
        ]);
        $airport->title = $request->title;
        $airport->airport_type = $request->airport_type;
        $airport->location = $request->location;
        $airport->save();
        return redirect(route('airports.index'))->with('msg', 'AirPort Updated Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AirPort  $airPort
     * @return \Illuminate\Http\Response
     */
    public function destroy(AirPort $airPort)
    {
        $airPort->delete();
        return redirect(route('airports.index'))->with('msg', 'AirPort deleted Successfuly');
    }
}
