<?php

namespace App\Listeners;

use App\Events\SendEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendActivationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmail  $event
     * @return void
     */
    public function handle(SendEmail $event)
    {
        $user = $event->user;
        if($user->user_level==2||$user->user_level==3) {
            $user->activation_code=uniqid(rand(1,99));
            $user->save();
            Mail::send('emails.agencyregister',
                array(
                    'user' => $user,
                ), function ($message) use ($user) {
                    $message->from('admin@urood.skywardbuilders.pk');
                    $message->to('admin@urood.skywardbuilders.pk')->subject('Account Activation');

                });
        }else{
            Mail::send('emails.userregister',
                array(
                    'user' => $user,
                ), function ($message) use ($user) {
                    $message->from('admin@urood.skywardbuilders.pk');
                    $message->to('admin@urood.skywardbuilders.pk')->subject('Account Activation');

                });
        }

    }
}
