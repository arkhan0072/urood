<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    public function package(){
        $this->belongsTo('App\Package');
    }
}
