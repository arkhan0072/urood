<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function hotel(){
    	$this->belonsTo('App\Hotel');
    }
    public function packages(){
        $this->belonsTo('App\Package');
    }
}
