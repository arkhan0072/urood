<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public function package(){
        $this->belongsTo('App\Package');
    }
}
