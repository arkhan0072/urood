@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2">
                <div class="db-2-com db-2-main">

                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db2.png')}}" alt="" />
                                <a href="{{route('packages.index')}}"><span>Packages</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db3.png')}}" alt="" />
                                <a href="{{route('hotels.index')}}"><span>Hotels</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db1.png')}}" alt="" />
                                <a href="{{route('airports.index')}}"><span>Airports</span></a>

                            </div>
                        </div>
                    </div>
                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db4.png')}}" alt="" />
                                <a href="{{route('countries.index')}}"><span>Countries</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db5.png')}}" alt="" />
                                <a href="{{route('users.index')}}"><span>Users</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db6.png')}}" alt="" />
                                <a href="{{route('pages.index')}}"><span>Pages</span></a>

                            </div>
                        </div>
                    </div>
                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db7.png')}}" alt="" />
                                <a href="{{route('home')}}"><span>Contact Mails</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db8.png')}}" alt="" />
                                <a href="{{route('home')}}"><span>Expert Review</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db9.png')}}" alt="" />
                                <a href="{{route('advertisments.index')}}"><span>Advertisements</span></a>

                            </div>
                        </div>
                    </div>
                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db10.png')}}" alt="" />
                                <a href="changepassword.html"><span>Change Password</span></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
@endsection
