@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
        <div class="db-2">
            <div class="tr-regi-form" style="width:80%;">
                <h4>Add Airport</h4>
                @if(session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif
                <form method="post"  action="{{route('airports.store')}}" class="col s12">
                    @csrf
                    <div class="row">
                        <div class="input-field col m8 s12">
                            <h5 style="text-align:left">Title</h5>
                            <input type="text" name="title" class="validate @error('title') is-invalid @enderror"
                                   value="{{ old('title') }}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="input-field col m4 s12">
                            <h5 style="text-align:left">Airport Type</h5>
                            <select name="airport_type">

                                <option value="1">Start</option>
                                <option value="2">End</option>

                            </select>
                            @error('airport_type')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <h5 style="text-align:left">Location</h5>
                            <input type="text" name="location" class="validate @error('location') is-invalid @enderror"
                                   value="{{ old('location') }}">
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                    </div>



                    <div class="row">
                        <div class="input-field col s12">
                            <input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</section>
<!--END DASHBOARD-->

    @endsection
