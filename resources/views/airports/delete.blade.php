<a href="#" onclick="frmdlt{{$airport->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$airport->id}}" action="{{ route('airports.destroy', $airport->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
