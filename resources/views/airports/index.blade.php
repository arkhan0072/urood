@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
        <div class="db-2" style="padding: 30px;">
            @if(session()->has('msg'))
                <div class="alert alert-success">
                    {{ session()->get('msg') }}
                </div>
            @endif
            <h4 style="text-align:center">Airports</h4>
            <div style="float:right;">
                <a href="{{route('airports.create')}}" class="btn btn-primary">Add AirPort</a>
            </div><br><br>
            <div >
                <table id="data-table" class="table table-bordered">
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Location</th>
                        <th>Airport Type</th>
                        <th>Created</th>
                        <th>Modified</th>
                        <th>Actions</th>
                    </tr>
{{--                    @foreach($airports as $airport)--}}
{{--                    <tr>--}}
{{--                        <td>{{$airport->id}}</td>--}}
{{--                        <td>{{$airport->title}}</td>--}}
{{--                        <td>{{$airport->location}}</td>--}}
{{--                        <td>{{$airport->airport_type == 1 ? "Start":"End"}}</td>--}}
{{--                        <td>{{$airport->created_at}}</td>--}}
{{--                        <td>{{$airport->updated_at}}</td>--}}
{{--                        <td><i class="fa fa-eye"></i>--}}
{{--                            <i class="fa fa-edit"></i>--}}
{{--                            <i class="fa fa-trash-o"></i>--}}
{{--                            <i class="fa fa-clone"></i></td>--}}
{{--                    </tr>--}}
{{--                        @endforeach--}}
                </table>
            </div>


        </div>

    </div>
</section>
@section('script')
    <script>
        $(function () {

            var table = $('#data-table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('ajaxAirport') }}",
                buttons: [
                    {{--{--}}
                    {{--    text: 'Add Airport',--}}
                    {{--    action: '{{route('airports.create')}}',--}}
                    {{--    className:'btn btn-primary'--}}
                    {{--},--}}
                    'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id',title:'Id'},
                    {data: 'title', name: 'title',title:'Title'},
                    {data: 'location', name: 'location',title:'Location'},
                    {data: 'airport_type', name: 'airport_type',title:'Airport Type'},
                    {data: 'created_at', name: 'airport_type',title:'Created'},
                    {data: 'updated_at', name: 'airport_type',title:'Modified'},
                    {data: 'action', name: 'action',title:'Action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
<!--END DASHBOARD-->
@endsection
