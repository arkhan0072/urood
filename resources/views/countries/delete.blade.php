<a href="#" onclick="frmdlt{{$country->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$country->id}}" action="{{ route('countries.destroy', $country->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
