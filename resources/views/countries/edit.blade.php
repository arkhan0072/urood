@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
			<div class="db-2">
				<div class="tr-regi-form" style="width:80%;">
				<h4>Edit Country</h4>
				@if(session()->has('msg'))
			    <div class="alert alert-success">
			        {{ session()->get('msg') }}
    			</div>
				@endif
				<form action="{{route('countries.update',$country->id)}}" class="col s12" method="post">
					@csrf
                    {{method_field('PATCH')}}
					<div class="row">
						<div class="input-field col m4 s12">
						<h5 style="text-align:left">Iso</h5>
							<input type="text" name="iso" class="validate @error('iso') is-invalid @enderror"
							value="{{ old('iso',$country->iso) }}">
							  @error('iso')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div class="input-field col m4 s12">
						<h5 style="text-align:left">Name</h5>
							<input type="text" name="name" class="validate @error('name') is-invalid @enderror"
							value="{{ old('name',$country->name) }}">
							  @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div class="input-field col m4 s12">
						<h5 style="text-align:left">Nice Name</h5>
							<input type="text" name="nice_name" class="validate @error('nice_name') is-invalid @enderror"
							value="{{ old('nice_name',$country->nice_name) }}">
							  @error('nice_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
					</div>


					<div class="row">
						<div class="input-field col m4 s12">
						<h5 style="text-align:left">Iso 3</h5>
							<input type="text" name="iso_3" class="validate @error('iso_3') is-invalid @enderror" value="{{ old('iso_3',$country->iso_3) }}">
							  @error('iso_3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
							<div class="input-field col m4 s12">
						<h5 style="text-align:left">Numcode</h5>
							<input type="number" name="numcode" class="validate @error('numcode') is-invalid @enderror" value="{{ old('numcode',$country->numcode) }}">
							  @error('numcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div class="input-field col m4 s12">
						<h5 style="text-align:left">Phonecode</h5>
							<input type="number" name="phonecode" class="validate @error('phonecode') is-invalid @enderror" value="{{ old('phonecode',$country->phonecode) }}">
							  @error('phonecode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>

					</div>



					<div class="row">
						<div class="input-field col s12">
							<input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
					</div>
				</form>

			</div>
			</div>

		</div>
	</section>
	<!--END DASHBOARD-->
@endsection
