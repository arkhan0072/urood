 @extends('master')
@section('content')
<style>
table {
  margin:10px;
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: center;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
     <!--DASHBOARD-->
     <section>
         <div class="db">
             <!--LEFT SECTION-->
         @include('leftbar')
         <!--CENTER SECTION-->
			<div class="db-2" style="padding: 30px;">
                @if(session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif
				<h4 style="text-align:center">Countries</h4>
                <div style="float:right;">
                    <a href="{{route('countries.create')}}" class="btn btn-primary">Add Country</a>
                </div><br><br>
				<div >
  <table class="table table-bordered" id="data-table">
    <tr>
      <th>Id</th>
      <th>Iso</th>
      <th>Name</th>
      <th>Nicename</th>
{{--	  <th>Iso3</th>--}}
	  <th>Numcode</th>
	  <th>Phonecode</th>
	  <th>Created</th>
	  <th>Modified</th>
	  <th>Actions</th>
    </tr>
{{--    @foreach($countries as $country)--}}
{{--    <tr>--}}
{{--      <td>{{$country->id}}</td>--}}
{{--      <td>{{$country->iso}}</td>--}}
{{--      <td>{{$country->name}}</td>--}}
{{--      <td>{{$country->nice_name}}</td>--}}
{{--	  <td>{{$country->iso_3}}</td>--}}
{{--	  <td>{{$country->numcode}}</td>--}}
{{--	  <td>{{$country->phonecode}}</td>--}}
{{--	  <td>{{$country->created_at}}</td>--}}
{{--	  <td>{{$country->updated_at}}</td>--}}
{{--	   <td><i class="fa fa-eye"></i>--}}
{{--	   <i class="fa fa-edit"></i>--}}
{{--	   <i class="fa fa-trash-o"></i>--}}
{{--	   <i class="fa fa-clone"></i></td>--}}
{{--    </tr>--}}
{{--    @endforeach--}}
  </table>
</div>


			</div>

		</div>
	</section>
	<!--END DASHBOARD-->
 @section('script')
     <script>
         $(function () {

             var table = $('#data-table').DataTable({
                 dom: 'Bfrtip',
                 processing: true,
                 serverSide: true,
                 ajax: "{{ route('ajaxCountry') }}",
                 buttons: [
                     {{--{--}}
                         {{--    text: 'Add Advertisements',--}}
                         {{--    action: '{{route('advertisments.create')}}',--}}
                         {{--    className:'btn btn-primary'--}}
                         {{--},--}}
                         'copy', 'excel', 'pdf'
                 ],
                 columns: [
                     {data: 'id', name: 'id',title:'Id'},
                     {data: 'iso', name: 'iso',title:'Iso'},
                     {data: 'name', name: 'name',title:'Name'},
                     {data: 'nice_name', name: 'nice_name',title:'Nicename'},
                     // {data: 'iso_3', name: 'iso_3',title:'Iso3'},
                     {data: 'numcode', name: 'numcode',title:'Numcode'},
                     {data: 'phonecode', name: 'phonecode',title:'Phonecode'},
                     {data: 'created_at', name: 'created_at',title:'Created'},
                     {data: 'updated_at', name: 'updated_at',title:'Modified'},
                     {data: 'action', name: 'action',title:'Action', orderable: false, searchable: false},
                 ]
             });

         });
     </script>
 @endsection
	@endsection
