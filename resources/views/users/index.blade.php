@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2" style="padding: 30px;">
                @if(session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif
                <h4 style="text-align:center">Users</h4>
                <div style="float:right;">
                    <a href="{{route('users.create')}}" class="btn btn-primary">Add User</a>
                </div>
                <br><br>

                <div style="overflow-x:auto;">
                    <table class="table table-bordered table-responsive" id="data-table">
                        <tr>
                            <th>Id</th>
                            {{--      <th>Role</th>--}}
                            <th>Country</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip</th>
                            <!-- <th>Url</th> -->
                            <th>Actions</th>
                        </tr>
                        {{--    @foreach($users as $user)--}}
                        {{--    <tr>--}}

                        {{--      <td>{{$user->id}}</td>--}}
                        {{--      <td><a href="#">USER</a></td>--}}
                        {{--      <td><a href="#">{{$user->country_id}}</a></td>--}}
                        {{--      <td>{{$user->fname}}</td>--}}
                        {{--      <td>{{$user->lname}}</td>--}}
                        {{--      <!-- <td></td> -->--}}
                        {{--      <td>{{$user->email}}</td>--}}
                        {{--      <td>{{$user->city}}</td>--}}
                        {{--      <td>{{$user->state}}</td>--}}
                        {{--      <td>{{$user->zipcode}}</td>--}}
                        {{--      <!-- <td>www.dareleiman.com</td> -->--}}
                        {{--	<td>--}}
                        {{--	   	<a href=""><i class="fa fa-eye"></i></a>--}}
                        {{--	   	<a href="{{route('users.edit',$user->id)}}"><i class="fa fa-edit"></i></a>--}}
                        {{--	    <a href="{{route('users.destroy',$user->id)}}"><i class="fa fa-trash-o"></i></a>--}}
                        {{--	</td>--}}
                        {{--    </tr>--}}
                        {{--    @endforeach--}}
                    </table>
                </div>


            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
 @section('script')
    <script>
        $(function () {

            var table = $('#data-table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('ajaxUser') }}",
                buttons: [
                    {{--{--}}
                        {{--    text: 'Add User',--}}
                        {{--    action: '{{route('users.create')}}',--}}
                        {{--    className:'btn btn-primary'--}}
                        {{--},--}}
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id', title: 'id'},
                    {data: 'country.name', name: 'name', title: 'Country'},
                    {data: 'fname', name: 'fname', title: 'First Name'},
                    {data: 'lname', name: 'lname', title: 'Last Name'},
                    {data: 'email', name: 'email', title: 'Email'},
                    {data: 'address', name: 'address', title: 'Address'},
                    {data: 'city', name: 'city', title: 'City'},
                    {data: 'state', name: 'state', title: 'State'},
                    {data: 'zipcode', name: 'zipcode', title: 'ZipCode'},
                    {data: 'action', name: 'action', title: 'Action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
 @endsection
@stop

