@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2">
                <style type="text/css">
                    .tr-regi-form {
                        box-shadow: 0px 0px 0px rgba(0, 0, 0, 0)!important;
                    }
                </style>
                <div class="tr-regi-form container" style="width:80%;">
                    <h4>Update User Profile</h4>
                    <form method="POST" action="{{ route('Updateprofile',$user->id) }}" class="col s12">
                        @csrf
                        {{method_field('PATCH')}}

                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input class="validate @error('fname') is-invalid @enderror"  id="fname" type="text" name="fname" value="{{ old('fname',$user->fname) }}" required autocomplete="fname" >
                                <label>First Name</label>
                                @error('fname')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m6 s12">
                                <input type="text" class="validate @error('lname') is-invalid @enderror" id="lname" type="text" name="lname" value="{{ old('lname',$user->lname) }}" required autocomplete="lname" >
                                <label>Last Name</label>
                                @error('lname')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input type="email" class="validate @error('email') is-invalid @enderror" id="email"
                                       name="email" value="{{ old('email',$user->email) }}" required autocomplete="email" >
                                <label>Email</label>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m6 s12">
                                <input type="password" id="password" name="password" class="validate @error('password') is-invalid @enderror" value="{{ old('password') }}" required >
                                <label>Password</label>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <span class="text-lighten-2">Only enter if you want to update your Password</span>
                            </div>
                            <div class="input-field col m12 s12">
                                <label>Confirm Password</label>
                                <input id="password-confirm" type="password" class="validate @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" class="validate @error('password') is-invalid @enderror" name="address" id="address" value="{{old('address',$user->address)}}">
                                <label>Address</label>
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input type="text" class="validate @error('city') is-invalid @enderror" name="city" id="city" value="{{old('city',$user->city)}}">
                                <label>City</label>
                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m6 s12">
                                <input type="text" class="validate @error('state') is-invalid @enderror" name="state" id="state" value="{{old('state',$user->state)}}">
                                <label>State</label>
                                @error('state')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input type="text" class="validate @error('zipcode') is-invalid @enderror" name="zipcode" id="zipcode" value="{{old('zipcode',$user->zipcode)}}">
                                <label>Zip</label>
                                @error('zipcode')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m6 s12">
                                <select name="country">
                                    <option value="" disabled selected>Select Country</option>
                                    @php
                                        $array=[1,2,3,4,5];
                                    @endphp
                                    @foreach($array as $a)
                                        <option value="{{$a}}" {{$a == $user->country_id ? 'selected':''}}>{{$a}}</option>

                                    @endforeach

                                </select>
                                @error('country')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m4 s12">
                                <input type="number" id="homenumber" value="{{old('homenumber',$user->homephone)}}" name="homenumber" class="validate  @error('homenumber') is-invalid @enderror">
                                <label>Home Phone</label>
                                @error('homenumber')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m4 s12">
                                <input type="number" id="mobilenumber" value="{{old('mobilenumber',$user->mobilephone)}}" name="mobilenumber" class="validate  @error('mobilenumber') is-invalid @enderror">
                                <label>Mobile Phone</label>
                                @error('mobilenumber')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m4 s12">
                                <input type="number" id="mobilenumber" value="{{old('officenumber',$user->officenumber)}}" name="officenumber" class="validate  @error('officenumber') is-invalid @enderror">
                                <label>Office Phone</label>
                                @error('officenumber')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input type="submit" value="submit" class="waves-effect waves-light btn-large full-btn"> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
@endsection
