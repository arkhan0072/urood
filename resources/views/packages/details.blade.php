@extends('master')
@section('content')
<section>
    <div class="rows inn-page-bg com-colo">
        <div class="container inn-page-con-bg tb-space">
            <div class="col-md-9">
                <!--====== TOUR TITLE ==========-->
                <div class="tour_head">
                    <h2>{{$package->title}}
                        <span class="tour_rat">chicagohajj</span>
                    </h2>
                </div>
                <!--====== TOUR DESCRIPTION ==========-->
                <div class="tour_head1 hotel-com-color">
                    <h3>Package Details</h3>
                    <div class="db-2-main-com db-2-main-com-table">
                        <table class="responsive-table">
                            <tbody>
                            <tr>
                                <td>Package Type</td>
                                <td>:</td>
                                <td>{{$package->packagetype}}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>:</td>
                                @foreach($package->price as $price)

                                <td>$ {{$price->price}} per person ({{$price->people_per_room}} people per room)</td>
                                    @endforeach
                            </tr>
                            <tr>
                                <td>Depart Date</td>
                                <td>:</td>
                                <td>{{$package->depart_date}}</td>
                            </tr>
                            <tr>
                                <td>Return Date</td>
                                <td>:</td>
                                <td>{{$package->return_date}}</td>
                            </tr>

                            <tr>
                                <td>Itinerary</td>
                                <td>:</td>
                                <td>{{$package->itineray}}<a href="#" style="color: goldenrod">Click here</a></td><!--  will open modal -->
                            </tr>
                            <tr>
                                <td>Total Days</td>
                                <td>:</td>
                                <td>{{$package->totaldays}}</td>
                            </tr>
                            <tr>
                                <td>Hotels</td>
                                <td>:</td>
                                <td>
                                    <p>
                                        <strong>Makkah Hotel: </strong>
                                        <a href="#" style="color: goldenrod">{{$package->hotelsmakha->name}}</a>&nbsp;{{$package->hotelsmakha->star}} stars
                                    </p>
                                    <p>
                                        <strong>Madinah Hotel: </strong>
                                        <a href="#" style="color: goldenrod">{{$package->madinahhotel->name}}</a>&nbsp;{{$package->madinahhotel->star}} stars
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>Ticket Includes</td>
                                <td>:</td>
                                <td>Airfare / Ticket not included</td>
                            </tr>
                            <tr>
                                <td>Starting City</td>
                                <td>:</td>
                                <td><a href="#" style="color: goldenrod">Makkah First Program</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!--====== AMENITIES ==========-->
                <div class="tour_head1 hot-ameni">
                    <h3 style="color: goldenrod">Features</h3>
                    <hr>
                    <ul>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> For more details please visit the agency's website.</li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Guidance throughout your trip by professional and knowledgeable guides. </li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> 24 hour assistance during your stay.</li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Our staff will assist you in: English, Arabic, Urdu, Hindi, Bengali, Punjabi and Gujarati.</li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Document Handling. </li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> No Surprises When You’re Ready to Pay Upfront pricing for packages means that the price quoted reflects all included taxes. Your total price is clearly displayed throughout the booking process, so you can easily see any revisions.</li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> We provide customize packages. There is nothing fixed. If you want anything to be changed plus or minus, we will organize for you.</li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Airfare is available as an additional charge. </li>
                    </ul>
                </div>

                <div class="tour_head1 hot-ameni">
                    <h3 style="color: goldenrod">Other</h3>
                    <hr>
                    <ul>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Guide language English</li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Guide language Arabic </li>
                        <li style="width: 100%"><i class="fa fa-check" aria-hidden="true"></i> Guide language Urdu</li>
                    </ul>
                </div>
                <!--====== ROOMS: HOTEL BOOKING ==========-->
                <div class="tour_head1 hotel-book-room">
                    <h3>Photo Gallery</h3>
                    <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators carousel-indicators-1">
                            <li data-target="#myCarousel1" data-slide-to="0"><img src="images/gallery/s1.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="1"><img src="images/gallery/s2.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="2"><img src="images/gallery/s3.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="3"><img src="images/gallery/s4.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="4"><img src="images/gallery/s5.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="5"><img src="images/gallery/s6.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="6"><img src="images/gallery/s7.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="7"><img src="images/gallery/s8.jpg" alt="Chania">
                            </li>
                            <li data-target="#myCarousel1" data-slide-to="8"><img src="images/gallery/s9.jpg" alt="Chania">
                            </li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner carousel-inner1" role="listbox">
                            <div class="item active"> <img src="images/gallery/s1.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s2.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s3.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s4.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s5.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s6.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s7.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s8.jpg" alt="Chania" width="460" height="345"> </div>
                            <div class="item"> <img src="images/gallery/s9.jpg" alt="Chania" width="460" height="345"> </div>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span><i class="fa fa-angle-left hotel-gal-arr" aria-hidden="true"></i></span> </a>
                        <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span><i class="fa fa-angle-right hotel-gal-arr hotel-gal-arr1" aria-hidden="true"></i></span> </a>
                    </div>
                </div>

                <div class="tour_head1 hotel-com-color">
                    <h3>About this Agency</h3>
                    <div class="db-2-main-com db-2-main-com-table">
                        <table class="responsive-table">
                            <tbody>
                            <tr>
                                <td><strong>Company:</strong></td>
                                <td>:</td>
                                <td>chicagohajj</td>
                            </tr>
                            <tr>
                                <td><strong>Address:</strong></td>
                                <td>:</td>
                                <td>CHICAGO 2818 W Peterson Ave</td>
                            </tr>

                            <tr>
                                <td><strong>City</strong></td>
                                <td>:</td>
                                <td>Chicago,</td>
                            </tr>
                            <tr>
                                <td><strong>State</strong></td>
                                <td>:</td>
                                <td><strong>IL</strong></td>
                            </tr>

                            <tr>
                                <td><strong>Zip</strong></td>
                                <td>:</td>
                                <td>60659</td>
                            </tr>
                            <tr>
                                <td><strong>Url</strong></td>
                                <td>:</td>
                                <td>http://www.chicagohajj.com</td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>:</td>
                                <td>
                                    <p>
                                        <strong>Phone: </strong>
                                        773-465-8500
                                    </p>
                                    <p>
                                        info@chicagohajj.com
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>Ticket Includes</td>
                                <td>:</td>
                                <td>Airfare / Ticket not included</td>
                            </tr>
                            <tr>
                                <td>Starting City</td>
                                <td>:</td>
                                <td><a href="#" style="color: goldenrod">Makkah First Program</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div>
                    <div class="dir-rat">
                        <div class="dir-rat-inn dir-rat-title">
                            <h3 style="color: goldenrod">More information</h3>
                            <p style="color: goldenrod">Get more information</p>
                        </div>
                        <div class="dir-rat-inn">
                            <form class="dir-rat-form">

                                <div class="form-group col-md-6 pad-left-o">
                                    <input type="text" class="form-control" id="email11" placeholder="Nimber of people traveling">
                                </div>
                                <div class="form-group col-md-6 pad-left-o">
                                    <input type="text" class="form-control" id="email11" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6 pad-left-o">
                                    <input type="number" class="form-control" id="email12" placeholder="Last Name"> </div>
                                <div class="form-group col-md-6 pad-left-o">
                                    <input type="email" class="form-control" id="email13" placeholder="Email"> </div>
                                <div class="form-group col-md-6 pad-left-o">
                                    <input type="text" class="form-control" id="email14" placeholder="Mobile"> </div>
                                <div class="form-group col-md-12 pad-left-o">
                                    <textarea placeholder="Write your message">I am interested to find out more about this package. I can be reached by email or phone.</textarea>
                                </div>
                                <div class="form-group col-md-12 pad-left-o">
                                    <input type="submit" value="Send Inquiry" class="link-btn"> </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-2 tour_r" style="text-align: right;">
                <img src="images/squaremouth-travel-insurance.jpg" alt="" />
            </div>
        </div>
    </div>
</section>
    @endsection


