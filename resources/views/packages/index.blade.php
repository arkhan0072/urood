@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2" style="padding: 30px;">
                @if(session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif
            <h4 style="text-align:center">Packages</h4>
                    <div style="float:right;">
                        <a href="{{route('packages.create')}}" class="btn btn-primary">Add Package</a>
                    </div><br><br>
            <div style="overflow-x:auto;">
                <table id="data-table" class="table table-responsive table-bordered">
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Package Type</th>
                        <th>Description</th>
                        <th>Depart</th>
                        <th>Return</th>
                        <th>Total Days</th>
                        <th>Makkah Hotel</th>
                        <th>Madinah Hotel</th>
                        <th>Agency</th>
                        <th>Status</th>
                        <th>Featured Above</th>
                        <th>Featured Below</th>
                        <th>Actions</th>
                    </tr>
{{--                    <tr>--}}
{{--                        <td>576</td>--}}
{{--                        <td>Vip Express (Madinah First - No Azizia - Room Access - VIP Tents)</td>--}}
{{--                        <td>Hajj</td>--}}
{{--                        <td>Vip Express (Madinah First - No Azizia - Room Access - VIP Tents)</td>--}}
{{--                        <td>08/04/2019</td>--}}
{{--                        <td>08/15/2019</td>--}}
{{--                        <td>11</td>--}}
{{--                        <td><a href="#">Swissotel Makkah</a></td>--}}
{{--                        <td><a href="#">Dallah Taibah Hotel</a></td>--}}
{{--                        <td><a href="#">Dawn Travels</a></td>--}}
{{--                        <td>Active</td>--}}
{{--                        <td><i class="fa fa-close"></i></td>--}}
{{--                        <td><i class="fa fa-close"></i></td>--}}
{{--                        <td><i class="fa fa-eye"></i>--}}
{{--                            <i class="fa fa-edit"></i>--}}
{{--                            <i class="fa fa-trash-o"></i>--}}
{{--                            <i class="fa fa-clone"></i></td>--}}
{{--                    </tr>--}}
                </table>
            </div>


        </div>

    </div>
</section>
<!--END DASHBOARD-->
    @section('script')
    <script>
        $(function () {

            var table = $('#data-table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('ajaxPackage') }}",
                buttons: [
                    {{--{--}}
                        {{--    text: 'Add User',--}}
                        {{--    action: '{{route('users.create')}}',--}}
                        {{--    className:'btn btn-primary'--}}
                        {{--},--}}
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id',title:'id'},
                    {data: 'title', name: 'title',title:'Title'},
                    {data: 'packagetype', name: 'packagetype',title:'Package Type'},
                    {data: 'description', name: 'description',title:'Description'},
                    {data: 'depart_date', name: 'depart_date',title:'Depart'},
                    {data: 'return_date', name: 'return_date',title:'Return'},
                    {data: 'totaldays', name: 'totaldays',title:'Total Days'},
                    {data: 'hotelsmakha.name', name: 'hotelsmakha.name',title:'Makkah Hotel'},
                    {data: 'madinahhotel.name', name: 'madinahhotel.name',title:'Madinah Hotel'},
                    {data: 'agency_id', name: 'agency_id',title:'Agency'},
                    {data: 'status', name: 'status',title:'Status'},
                    {data: 'appearsabovesearch', name: 'appearsabovesearch',title:'Featured Above'},
                    {data: 'appearsbelowsearchbox', name: 'appearsbelowsearchbox',title:'Featured Below'},
                    // {data: 'zipcode', name: 'zipcode',title:'ZipCode'},
                    {data: 'action', name: 'action',title:'Action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
        @endsection
    @endsection
