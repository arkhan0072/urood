@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2">
                <div class="tr-regi-form" style="width:80%;">
                    <h4>Edit Packages</h4>

                    <form action="{{route('packages.update',$package->id)}}" class="col s12" method="post" enctype="multipart/form-data">
                        @csrf
                        {{method_field('PATCH')}}
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <h5 style="text-align:left">Package Type</h5>
                                <select class="form-control" name="packagetype">
                                    <option value="" readonly="readonly">Package Type</option>
                                    <option value="Hajj" {{$package->package_type=='Hajj' ? 'selected=selected':''}}>Hajj</option>
                                    <option value="Umrah" {{$package->package_type=='Umrah' ? 'selected=selected':''}}>Umrah</option>

                                </select>
                            </div>
                            <div class="input-field col m6 s12">
                                <h5 style="text-align:left">Title</h5>
                                <input class="validate @error('title') is-invalid @enderror"  id="title" type="text" name="title" value="{{ old('title',$package->title) }}" required autocomplete="title" >
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <h5 style="text-align:left">Description</h5>
                                <input class="validate @error('description') is-invalid @enderror"  id="description" type="text" name="description" value="{{ old('description',$package->description) }}" required autocomplete="description" >
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row appendData">
                            @foreach($package->price as $price)
                            <div class="removeData">
                                <div class="input-field col m5 s12">
                                    <h5 style="text-align:left">Price</h5>
                                    <input class="validate @error('price') is-invalid @enderror"  id="description" type="number" name="price[]" value="{{ old('price',$price->price) }}" required autocomplete="price" >
                                    @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="input-field col m5 s12">
                                    <h5 style="text-align:left">People per room</h5>
                                    <input class="validate @error('people_per_room') is-invalid @enderror"  id="description" type="number" name="people_per_room[]" value="{{ old('people_per_room',$price->people_per_room) }}" required autocomplete="people_per_room" >
                                    @error('people_per_room')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>

                                <div class="input-field col m2 s12">
                                    <h5>To add more</h5>
                                    <button class="btn btn-default add-more" type="button"><i class="fa fa-plus-circle"></i></button>
                                    <button class="btn btn-default remove-more" type="button"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>
                                @endforeach
                        </div>
                        <div class="row">

                            <div class="input-field col m6 s12">
                                <h5 style="text-align:left">Agency</h5>
                                <select name="agency">
                                    <option value="" readonly>Select Agency</option>
                                    <option value="1" {{$package->agency_id==1 ? 'selected':''}}>Agency</option>
                                    <option value="2" {{$package->agency_id==2 ? 'selected':''}}>5 Star Hajj&Umrah Travels</option>
                                    <option value="3" {{$package->agency_id==3 ? 'selected':''}}>agency1</option>
                                </select>
                            </div>
                            <div class="input-field col m6 s12">
                                <h5 style="text-align:left">Number of Package Available</h5>
                                <input class="validate @error('numberofpackages') is-invalid @enderror"  id="numberofpackages" type="number" name="numberofpackages" value="{{ old('numberofpackages',$package->numberofpackages) }}" required autocomplete="numberofpackages" >
                                @error('people_per_room')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>


                        <div class="row">

                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Depart</h5>
                                <input type="date" name="depart_date" class="validate @error('depart_date') is-invalid @enderror" value="{{old('depart_date',\Carbon\Carbon::parse($package->depart_date)->format('Y-m-d'))}}"  id="first" >
                                @error('depart_date')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Return</h5>
                                <input type="date" name="return_date" class="validate @error('return_date') is-invalid @enderror" value="{{old('return_date',\Carbon\Carbon::parse($package->return_date)->format('Y-m-d'))}}"  id="second" >
                                @error('return_date')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Total Days</h5>
                                <input type="number" readonly="readonly" name="totaldays" class="form-control validate @error('totaldays') is-invalid @enderror" value="{{old('totaldays',$package->totaldays)}}"   id="totaldays" >
                                @error('totaldays')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s12">
                                <h5 style="text-align:left">Itineray</h5>
                                <textarea name="itineray" id="editor">{{$package->itineray}}</textarea>
                            </div>
                        </div>

                        <div class="row ticket-table">
                            <h5 style="text-align:left">Tickets</h5>
                            <div class="row datashow">
                                <div class="input-field col m4 s12">
                                    <h5 style="text-align:left">Start</h5>
                                    <select name="startairport_id[]" class="start_airport">
                                        @foreach($startAirports as $start)
                                            <option value="{{$start->id}}" {{$start->id == $package->startairport_id ? 'selected':''}}>{{$start->title}}</option>

                                        @endforeach
                                    </select>
                                </div>

                                <div class="input-field col m4 s12">
                                    <h5 style="text-align:left">End</h5>
                                    <select name="endairport_id[]" class="end_airport">
                                        @foreach($endAirports as $end)
                                            <option value="{{$end->id}}" {{$end->id == $package->endairport_id ? 'selected':''}}>{{$end->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="input-field col m2 s12">
                                    <h5>Round Trip</h5>
                                    <input class="styled roundtrip" name="roundtrip[]" {{$package->roundtrip == 1 ? 'checked':''}}  value="1" type="checkbox" style="opacity:1;left:20px;">
                                </div>
                                <div class="input-field col m2 s12">
                                    <button class="btn btn-default addticket" type="button" style="margin-top: 30px;"><i class="fa fa-plus-circle"></i></button>
                                    <button class="btn btn-default removeticket" type="button" style="margin-top: 30px;"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="input-field col s12 checkbox">
                                <h5 style="text-align:left">Ticket not Included</h5>
                                <input id="disablefields" class="styled" value="1" {{$package->ticketNotincluded == 1 ? 'checked':''}} name="ticketNotincluded" type="checkbox" style="opacity:1;left:10px;">
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Makkah Hotel</h5>
                                <select name="makkahhotel_id">
                                    @foreach($MakkahHotels as $makkahhotel)
                                        <option value="{{$makkahhotel->id}}" {{$makkahhotel->id == $package->makkahhotel_id ? 'selected':''}}>{{$makkahhotel->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Madinnah Hotel</h5>
                                <select name="madinahhotel_id">
                                    @foreach($MadinahHotels as $madinahotel)
                                        <option value="{{$madinahotel->id}}" {{$madinahotel->id == $package->madinahhotel_id ? 'selected':''}}>{{$madinahotel->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Starting City</h5>
                                <select name="startingcity">
                                    <option value="Makkah" {{$package->startingcity == 'Makkah' ? 'selected':''}}>Makkah</option>
                                    <option value="Madinah" {{$package->startingcity == 'Madinah' ? 'selected':''}}>Madinah</option>
                                </select>
                            </div>
                        </div>


                        <div class="row">

                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Side Trip</h5>
                                <select name="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" {{$package->country_id == $country->id ? 'selected':''}}>{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Side Trip Start Date</h5>
                                <input type="date" name="side_trip_start_date" value="{{old('side_trip_start_date',\Carbon\Carbon::parse($package->side_trip_start_date)->format('Y-m-d'))}}" class="validate">
                            </div>
                            <div class="input-field col m4 s12">
                                <h5 style="text-align:left">Side Trip End Date</h5>
                                <input type="date" name="side_trip_end_date" value="{{old('side_trip_end_date',\Carbon\Carbon::parse($package->side_trip_end_date)->format('Y-m-d'))}}" class="validate">
                            </div>
                        </div>


                        <div class="row showdata">
                            <h5 style="text-align:left">Upload Photo</h5>
                            <div class="data_append">
                                <div class="input-field col m4 s12">
                                    <input type="text" name="pictures[]" class="validate">
                                </div>

                                <div class="input-field col m6 s12">

                                    <input type="file" name="pic[]" accept="image/*">

                                </div>

                                <div class="input-field col m2 s12">
                                    <button class="btn btn-default addnew" type="button"><i class="fa fa-plus-circle"></i></button>
                                    <button class="btn btn-default remove" type="button"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 checkbox" style="text-align:left">

                                <input id="chp3" class="styled" type="checkbox" name="status" value="1" style="opacity:1;left:0">Active
                            </div>
                        </div>

                        <div class="row">

                            <div class="input-field col s12 checkbox" style="text-align:left">
                                <h5 style="text-align:left">Package Features</h5>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->air_condition_bus == 1 ? 'checked':''}} id="chp4" name="air_condition_bus" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Air Conditioned Bus</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->buffet_breakfast_during_hotel_stay_in_madinah == 1 ? 'checked':''}} id="chp5" name="buffet_breakfast_during_hotel_stay_in_madinah" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Buffet Breakfast During Hotel Stay In Madinah</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->buffet_breakfast_during_hotel_stay_in_makkah == 1 ? 'checked':''}} id="chp6" name="buffet_breakfast_during_hotel_stay_in_makkah" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Buffet Breakfast During Hotel Stay In Makkah</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->food_in_mina == 1 ? 'checked':''}} id="chp7" name="food_in_mina" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Food In Mina</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->food_in_arafat == 1 ? 'checked':''}} id="chp8" name="food_in_arafat" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Food In Arafat</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->no_food_included_in_the_package == 1 ? 'checked':''}} id="chp9" name="no_food_included_in_the_package" value="1" class="styled" type="checkbox" style="opacity:1;left:0">No Food Included In The Package</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->regular_american_tents == 1 ? 'checked':''}} id="chp10" name="regular_american_tents" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Regular American Tents</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->regular_air_conditioned_in_mina == 1 ? 'checked':''}} id="chp11" name="regular_air_conditioned_in_mina" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Regular Air Conditioned In Mina</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->camp_in_front_of_the_jamarat_mena == 1 ? 'checked':''}} id="chp12" name="camp_in_front_of_the_jamarat_mena" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Camp In Front Of The Jamarat Mena</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->educational_program_english == 1 ? 'checked':''}} id="chp13" name="educational_program_english" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Educational Program English</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->educational_program_arabic == 1 ? 'checked':''}} id="chp14" name="educational_program_arabic" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Educational Program Arabic</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->educational_program_urdu == 1 ? 'checked':''}} id="chp15" name="educational_program_urdu" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Educational Program Urdu</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->guide_language_english == 1 ? 'checked':''}} id="chp16" name="guide_language_english" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Guide Language English</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->guide_language_arabic == 1 ? 'checked':''}} id="chp17" name="guide_language_arabic" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Guide Language Arabic</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->guide_language_urdu == 1 ? 'checked':''}} id="chp18" name="guide_language_urdu" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Guide Language Urdu</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->literature_english == 1 ? 'checked':''}} id="chp19" name="literature_english" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Literature English</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->literature_arabic == 1 ? 'checked':''}} id="chp20" name="literature_arabic" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Literature Arabic</div>
                                <div class="input-field col m4 s12 checkbox" style="text-align:left"><input {{$package->literature_urdu == 1 ? 'checked':''}} id="chp21" name="literature_urdu" value="1" class="styled" type="checkbox" style="opacity:1;left:0">Literature Urdu</div>
                            </div>
                        </div>
                        <div class="row appendFeatures">
                            <h5 style="text-align:left">Features</h5>
                            <div class="addFeature">
                                <div class="input-field col m10 s12">
                                    <input type="text" name="name[]" class="validate">
                                </div>
                                <div class="input-field col m2 s12">
                                    <button class="btn btn-default addFeatures" type="button"><i class="fa fa-plus-circle"></i></button>
                                    <button class="btn btn-default removeFeatures" type="button"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </section>
@section('script')
    <Script>

        $('#second').change(function () {
            var startDate = Date.parse($('#first').val());
            var endDate = Date.parse($('#second').val());
            var timeDiff = endDate - startDate;
            daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
            $('#totaldays').val(daysDiff);
            console.log(daysDiff);
        });
        $('#first').change(function () {
            var startDate = Date.parse($('#first').val());
            var endDate = Date.parse($('#second').val());
            var timeDiff = endDate - startDate;
            daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
            $('#totaldays').val(daysDiff);
            console.log(daysDiff);
        });
        $('#disablefields').on('change', function () {
            if ($(this).is(':checked')) {
                $('.ticket-table').find('input, select').attr('disabled', 'disabled');
                $('.ticket-table').find('button').attr('disabled', 'disabled');
            } else {
                $('.ticket-table').find('input, select').removeAttr('disabled', 'disabled');
                $('.ticket-table').find('button').removeAttr('disabled', 'disabled');
            }
        });
        if ($('#disablefields').is(':checked')) {
            $('.ticket-table').find('input, select').attr('disabled', 'disabled');
            $('.ticket-table').find('button').attr('disabled', 'disabled');
        }
        $(document).on('click','.add-more',function (e) {
            e.preventDefault();
            $('.appendData').append("<div class=\"removeData\">\n" +
                "                            <div class=\"input-field col m5 s12\">\n" +
                "                                <h5 style=\"text-align:left\">Price</h5>\n" +
                "                                <input class=\"validate @error('price') is-invalid @enderror\"  id=\"description\" type=\"number\" name=\"price[]\" value=\"{{ old('price') }}\" required autocomplete=\"price\" >\n" +
                "                                @error('price')\n" +
                "                                <span class=\"invalid-feedback\" role=\"alert\">\n" +
                "                                        <strong>{{ $message }}</strong>\n" +
                "                                    </span>\n" +
                "                                @enderror\n" +
                "                            </div>\n" +
                "\n" +
                "                            <div class=\"input-field col m5 s12\">\n" +
                "                                <h5 style=\"text-align:left\">People per room</h5>\n" +
                "                                <input class=\"validate @error('people_per_room') is-invalid @enderror\"  id=\"description\" type=\"number\" name=\"people_per_room[]\" value=\"{{ old('people_per_room') }}\" required autocomplete=\"people_per_room\" >\n" +
                "                                @error('people_per_room')\n" +
                "                                <span class=\"invalid-feedback\" role=\"alert\">\n" +
                "                                        <strong>{{ $message }}</strong>\n" +
                "                                    </span>\n" +
                "                                @enderror\n" +
                "\n" +
                "                            </div>\n" +
                "\n" +
                "                            <div class=\"input-field col m2 s12\">\n" +
                "                                <h5>To add more</h5>\n" +
                "                                <button class=\"btn btn-default add-more\" type=\"button\"><i class=\"fa fa-plus-circle\"></i></button>\n" +
                "                                <button class=\"btn btn-default remove-more\" type=\"button\"><i class=\"fa fa-minus-circle\"></i></button>\n" +
                "                            </div>\n" +
                "                        </div>")

        });


        $(document).click(function () {

            $('.remove-more').click(function (e) {
                e.preventDefault();
                $(this).closest('.removeData').remove();

            });

        });
        $(document).on('click','.addnew',function(e){
            e.preventDefault();
            $('.showdata').append(' <div class="data_append">\n' +
                '                            <div class="input-field col m4 s12">\n' +
                '                                <input type="text" name="pictures[]" class="validate">\n' +
                '                            </div>\n' +
                '\n' +
                '                            <div class="input-field col m6 s12">\n' +
                '\n' +
                '                                 <input type="file" name="pic[]" accept="image/*">\n' +
                '\n' +
                '                            </div>\n' +
                '\n' +
                '                            <div class="input-field col m2 s12">\n' +
                '                                <button class="btn btn-default addnew" type="button"><i class="fa fa-plus-circle"></i></button>\n' +
                '                                <button class="btn btn-default remove" type="button"><i class="fa fa-minus-circle"></i></button>\n' +
                '                            </div>\n' +
                '                        </div>');

        });
        $(document).click(function () {
            $('.remove').click(function (e) {
                e.preventDefault();
                $(this).closest('.data_append').remove();

            });
        });
        $(document).on('click','.addticket',function () {
            $('.ticket-table').append(" <div class=\"row datashow\">\n" +
                "                        <div class=\"input-field col m4 s12\">\n" +
                "                            <h5 style=\"text-align:left\">Start</h5>\n" +
                "                            <select name=\"startairport_id[]\" class=\"form-control start_airport\">\n" +
                "                                @foreach($startAirports as $start)\n" +
                "                                    <option value=\"{{$start->id}}\">{{$start->title}}</option>\n" +
                "\n" +
                "                                @endforeach\n" +
                "                            </select>\n" +
                "                        </div>\n" +
                "\n" +
                "                        <div class=\"input-field col m4 s12\">\n" +
                "                            <h5 style=\"text-align:left\">End</h5>\n" +
                "                            <select name=\"endairport_id[]\" class=\"form-control end_airport\">\n" +
                "                                @foreach($endAirports as $end)\n" +
                "                                    <option value=\"{{$end->id}}\">{{$end->title}}</option>\n" +
                "\n" +
                "                                @endforeach\n" +
                "                            </select>\n" +
                "                        </div>\n" +
                "\n" +
                "                        <div class=\"input-field col m2 s12\">\n" +
                "                            <h5>Round Trip</h5>\n" +
                "                            <input class=\"styled roundtrip\" name=\"roundtrip[]\" value=\"1\" type=\"checkbox\" style=\"opacity:1;left:20px;\">\n" +
                "                        </div>\n" +
                "                        <div class=\"input-field col m2 s12\">\n" +
                "                            <button class=\"btn btn-default addticket\" type=\"button\" style=\"margin-top: 30px;\"><i class=\"fa fa-plus-circle\"></i></button>\n" +
                "                            <button class=\"btn btn-default removeticket\" type=\"button\" style=\"margin-top: 30px;\"><i class=\"fa fa-minus-circle\"></i></button>\n" +
                "                        </div>\n" +
                "                    </div>");
        });
        $(document).click(function () {
            $('.removeticket').click(function (e) {
                e.preventDefault();
                $(this).closest('.datashow').remove();

            });
        });

        $(document).on('click','.addFeatures',function (e) {
            e.preventDefault();
            $('.appendFeatures').append(' <div class="addFeature">\n' +
                '                        <div class="input-field col m10 s12">\n' +
                '                            <input type="text" name="name[]" class="validate">\n' +
                '                        </div>\n' +
                '                        <div class="input-field col m2 s12">\n' +
                '                            <button class="btn btn-default addFeatures" type="button"><i class="fa fa-plus-circle"></i></button>\n' +
                '                            <button class="btn btn-default removeFeatures" type="button"><i class="fa fa-minus-circle"></i></button>\n' +
                '                        </div>\n' +
                '                      </div>')

        });
        $(document).click(function () {
            $('.removeFeatures').click(function (e) {
                e.preventDefault();
                $(this).closest('.addFeature').remove();

            });
        });

    </script>
@endsection
<!--END DASHBOARD-->
@endsection
