<a href="#" onclick="frmdlt{{$package->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$package->id}}" action="{{ route('packages.destroy', $package->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
