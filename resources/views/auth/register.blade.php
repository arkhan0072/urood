
@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="tr-register">
            <div class="tr-regi-form">
            <div class="row">
                        <div class="input-field col m6 s12">
                            <h3>Member Registration</h3>
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="submit" value="Back" class="waves-effect waves-light btn-large ">
                        </div>
                    </div>


                    <form method="POST" action="{{ route('register') }}" class="col s12">
                        @csrf
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input class="validate @error('fname') is-invalid @enderror"  id="fname" type="text" name="fname" value="{{ old('fname') }}" required autocomplete="fname" >
                            <label>First Name</label>
                             @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="text" class="validate @error('lname') is-invalid @enderror" id="lname" type="text" name="lname" value="{{ old('lname') }}" required autocomplete="lname" >
                            <label>Last Name</label>
                             @error('lname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="email" class="validate @error('email') is-invalid @enderror" id="email"
                             name="email" value="{{ old('email') }}" required autocomplete="email" >
                            <label>Email</label>
                             @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="password" id="password" name="password" class="validate @error('password') is-invalid @enderror" value="{{ old('password') }}" required >
                            <label>Password</label>
                             @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="input-field col m12 s12">
                            <label>Confirm Password</label>
                            <input id="password-confirm" type="password" class="validate @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">

                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" class="validate @error('password') is-invalid @enderror" name="address" id="address" value="{{old('address')}}">
                            <label>Address</label>
                             @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="text" class="validate @error('city') is-invalid @enderror" name="city" id="city" value="{{old('city')}}">
                            <label>City</label>
                             @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="text" class="validate @error('state') is-invalid @enderror" name="state" id="state" value="{{old('state')}}">
                            <label>State</label>
                            @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                        <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="text" class="validate @error('zipcode') is-invalid @enderror" name="zipcode" id="zipcode" value="{{old('zipcode')}}">
                            <label>Zip</label>
                              @error('zipcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="input-field col m6 s12">
                            <select name="country">
                                        <option value="" disabled selected>Select Country</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>

                                        @endforeach

                                    </select>
                            @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="number" id="homenumber" value="{{old('homenumber')}}" name="homenumber" class="validate  @error('homenumber') is-invalid @enderror">
                            <label>Home Phone</label>
                            @error('homenumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="number" id="mobilenumber" value="{{old('mobilenumber')}}" name="mobilenumber" class="validate  @error('mobilenumber') is-invalid @enderror">
                            <label>Mobile Phone</label>
                             @error('mobilenumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                        <input type="hidden" name="user_level" value="1">

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="submit" value="submit" class="waves-effect waves-light btn-large full-btn"> </div>
                    </div>
                </form>
                <p>Are you a already member ? <a href="login.html">Click to Login</a>
                </p>
            </div>
        </div>
    </section>
    <!--END DASHBOARD-->

@endsection

