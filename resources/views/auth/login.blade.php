
@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="tr-register" style="background-color:#e6b905 !important;">
            <div class="tr-regi-form">
                <h4>Login</h4>
                <form method="POST"  class="col s12" action="{{ route('login') }}">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="email" class="validate @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <label>Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="password" type="password" class="validate @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <label>Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="submit" value="submit" class="waves-effect waves-light btn-large full-btn"> </div>
                    </div>
                </form>
                <p><a href="{{ route('password.request') }}">forgot password</a> | Do you have an account ? <a href="{{route('register')}}">Sign Up</a>
                </p>

            </div>
        </div>
    </section>
    <!--END DASHBOARD-->

@endsection
