@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
            @include('leftbar')
            <!--CENTER SECTION-->
            <div class="db-2">
                <div class="db-2-com db-2-main">

                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db2.png')}}" alt="" />
                                <a href="{{route('packages.index')}}"><span>Packages</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db10.png')}}" alt="" />
                                <a href="{{route('changepassword')}}"><span>Change Password</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/list.png')}}" alt="" />
                                <a href="{{route('orderD')}}"><span>Order Details</span></a>

                            </div>
                        </div>
                    </div>
                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2">
                                <a href="#"><span>Stats</span></a>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p style="font-size:20px; color:#23527c; padding-left:7px"><b>User Who Buy</b></p>
                                    <p style="font-size:20px; padding-left:7px">5</p>
                                </div>
                                <div class="col-md-6">
                                    <p style="font-size:20px; color:#23527c; padding-left:5px"><b>User Who Contact</b></p>
                                    <p style="font-size:20px; padding-left:7px">4</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
    @endsection
