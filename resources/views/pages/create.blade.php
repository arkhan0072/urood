@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
        <div class="db-2">
            <div class="tr-regi-form" style="width:80%;">
                <h4>Add Pages</h4>

                <form class="col s12" method="post" action="{{route('pages.store')}}">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <h5 style="text-align:left">Title</h5>
                            <input type="text" name="title" class="validate @error('title') is-invalid @enderror"
                                             value="{{ old('title') }}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <h5 style="text-align:left">Content</h5>
                            <textarea name="details" id="editor"></textarea>
                            @error('details')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="input-field col s12 checkbox" style="text-align:left">
                        <input id="chp3" class="styled" type="checkbox" value="1" name="showmenu" style="opacity:1;left:0">Show in Menu
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</section>
<!--END DASHBOARD-->
@endsection
