@extends('master')
@section('content')
    <!--DASHBOARD-->

    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
        <div class="db-2" style="padding: 30px;">
            @if(session()->has('msg'))
                <div class="alert alert-success">
                    {{ session()->get('msg') }}
                </div>
            @endif
            <h4 style="text-align:center">Pages</h4>
            <div style="float:right;">
                <a href="{{route('pages.create')}}" class="btn btn-primary">Add Page</a>
            </div><br><br>
            <div>
                <table class="data-table table table-bordered">
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Content</th>
                        <th>Show in menu</th>
                        <th>Created</th>
                        <th>Modified</th>
                        <th>Actions</th>
                    </tr>
{{--                    @foreach($pages as $page)--}}
{{--                    <tr>--}}
{{--                        <td>{{$page->id}}</td>--}}
{{--                        <td>{{$page->title}}</td>--}}
{{--                        <td>{{$page->slug}}</td>--}}
{{--                        <td>{{$page->content}}</td>--}}
{{--                        <td>{{$page->showmenu == 1 ? 'Yes':'No'}}</td>--}}
{{--                        <td>{{$page->created_at}}</td>--}}
{{--                        <td>{{$page->updated_at}}</td>--}}
{{--                        <td>--}}
{{--                                <i class="fa fa-eye"></i>--}}
{{--                            <i class="fa fa-edit"></i>--}}
{{--                            <i class="fa fa-trash-o"></i>--}}
{{--</td>--}}
{{--                    </tr>--}}
{{--                        @endforeach--}}
                </table>
            </div>


        </div>

    </div>
</section>
    @section('script')
    <script>
            $(function () {

                var table = $('.data-table').DataTable({
                    dom: 'Bfrtip',
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('ajaxPage') }}",
                    buttons: [
                        {{--{--}}
                            {{--    text: 'Add Hotel',--}}
                            {{--    action: '{{route('hotels.create')}}',--}}
                            {{--    className:'btn btn-primary'--}}
                            {{--    },--}}
                            'copy', 'excel', 'pdf'
                    ],
                    columns: [
                        {data: 'id', name: 'id',title:'id'},
                        {data: 'title', name: 'title',title:'Title'},
                        {data: 'slug', name: 'slug',title:'Content'},
                        {data: 'content', name: 'content',title:'Content'},
                        {data: 'showmenu', name: 'showmenu',title:'Show Menu'},
                        {data: 'created_at', name: 'created_at',title:'Created'},
                        {data: 'updated_at', name: 'updated_at',title:'Modified'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });

            });
    </script>
    @endsection
<!--END DASHBOARD-->
    @endsection
