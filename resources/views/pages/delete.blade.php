<a href="#" onclick="frmdlt{{$page->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$page->id}}" action="{{ route('pages.destroy', $page->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
