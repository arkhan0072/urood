@extends('master')

@section('content')
    @include('includes.headerSearch')
    <section>
        <div class="rows pad-bot-redu tb-space">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title">
                    <h2><span>Premium </span>Packages</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>

                </div>
                <div>
                @foreach($premium_packages as $premium)
                    <!-- TOUR PLACE 1 -->
                        <div class="col-md-4 col-sm-6 col-xs-12 b_packages">
                            <!-- OFFER BRAND -->
                            <!-- IMAGE -->
                            @if(sizeof($premium->photos)>0)
                                <div class="v_place_img"><img src="{{asset('public/images/packages')}}/{{$premium->photos[0]->name}}" alt="Tour Booking" title="Tour Booking"/>
                                </div>
                                @else
                                <div class="v_place_img"><img src="{{asset('public/images/default/1.jpeg')}}" alt="Tour Booking" title="Tour Booking"/>
                                </div>
                        @endif
                        <!-- TOUR TITLE & ICONS -->
                            <div class="b_pack rows">
                                <!-- TOUR TITLE -->
                                <div class="col-md-8 col-sm-8">
                                    <h4><a href="{{route('getPackage',$premium->id)}}">{{$premium->title}}</a></h4></div>
                                <!-- TOUR ICONS -->
                                <div class="col-md-4 col-sm-4 pack_icon">
                                    <ul>
                                        <li>
                                            <a href="#">{{$premium->price[0]->price}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--                <!-- TOUR PLACE 2 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 3 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 4 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 5 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 6 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="rows pad-bot-redu tb-space">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title">
                    <h2><span>Standard </span>Packages</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>

                </div>
                <div>
                @foreach($regular_packages as $regular)
                    <!-- TOUR PLACE 1 -->
                        <div class="col-md-4 col-sm-6 col-xs-12 b_packages">
                            <!-- OFFER BRAND -->
                            <!-- IMAGE -->
                            {{--                        {{$regular}}--}}
                            {{--                        @foreach($regular->photos as $photo)--}}
                            @if(sizeof($regular->photos)>0)
                                <div class="v_place_img"><img src="{{asset('public/images/packages')}}/{{$regular->photos[0]->name}}" alt="Tour Booking" title="Tour Booking"/>
                                </div>
                            @else
                                <div class="v_place_img"><img src="{{asset('public/images/default/1.jpeg')}}" alt="Tour Booking" title="Tour Booking"/>
                                </div>
                        @endif
                        {{--                        @endforeach--}}
                        <!-- TOUR TITLE & ICONS -->
                            <div class="b_pack rows">
                                <!-- TOUR TITLE -->
                                <div class="col-md-8 col-sm-8">
                                    <h4><a href="{{route('getPackage',$regular->id)}}">{{$regular->title}}</a></h4></div>
                                <!-- TOUR ICONS -->
                                <div class="col-md-4 col-sm-4 pack_icon">
                                    <ul>
                                        <li>
                                            {{--                                        @foreach($regular->price as $price)--}}
                                            {{--                                            @if($price->package_id == $regular->id)--}}
                                            <a href="#">{{$regular->price[0]->price}}</a>
                                            {{--                                            @endif--}}
                                            {{--                                            @endforeach--}}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                @endforeach
                <!-- TOUR PLACE 2 -->
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 3 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 4 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 5 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <!-- TOUR PLACE 6 -->--}}
                    {{--                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">--}}
                    {{--                        <!-- OFFER BRAND -->--}}
                    {{--                        <!-- IMAGE -->--}}
                    {{--                        <div class="v_place_img"><img src="images/ramdan.jpeg" alt="Tour Booking" title="Tour Booking"/>--}}
                    {{--                        </div>--}}
                    {{--                        <!-- TOUR TITLE & ICONS -->--}}
                    {{--                        <div class="b_pack rows">--}}
                    {{--                            <!-- TOUR TITLE -->--}}
                    {{--                            <div class="col-md-8 col-sm-8">--}}
                    {{--                                <h4><a href="tour-details.html">Ramadan Economy Umrah Package 2019 (3rd Ashra) Makkah--}}
                    {{--                                        First</a></h4></div>--}}
                    {{--                            <!-- TOUR ICONS -->--}}
                    {{--                            <div class="col-md-4 col-sm-4 pack_icon">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <a href="#">$1050</a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="ho-popu tb-space pad-bot-redu">
            <div class="rows container">
                <!-- TITLE & DESCRIPTION -->

                <div class="ho-popu-bod">
                    <div class="col-md-8">
                        <h2>Package Review</h2>

                        <img src="{{asset('images/video.png')}}" alt="">

                        <p>Get best travel deals for hotels, holidays, trains.Donec eget odio a nunc lacinia rhoncus.
                            Vivamus dignissim magna quis nisl fermentum porttitor. Maecenas venenatis mattis dui at
                            pharetra.Nullam rutrum odio eu viverra accumsan. Nam orci elit, scelerisque eget dui non,
                            sagittis tristique metus. Fusce nec nulla dapibus, fringilla nulla eu, vehicula odio. </p>
                        <div class="ho-con-cont"><a href="hotels-list.html" class="to-con-3 link-btn">Vip Express
                                (Madinah First - No Azizia - Room Access - VIP Tents)</a></div>
                        <div style="margin-top:15px;">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/ir8yS8t6yhM"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-md-4" style="margin-bottom: 2em;">
                        <div class="hot-page2-hom-pre-head">
                            <h4>Find Hajj/Umrah Agencies near you </h4></div>
                        <div class="">
                            <form class="contact__form v2-search-form" method="post"
                                  action="http://rn53themes.net/themes/demo/travelz/mail/carbooking.php">

                                <div class="row">
                                    <div class="input-field col s12">

                                        <input type="text" class="validate" name="name" required>
                                        <label>Agency name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="number" class="validate" name="phone" required>
                                        <label>State</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="submit" value="Submit"
                                               class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 2em;">
                        <div class="hot-page2-hom-pre-head">
                            <h4>Compare Hajj/Umrah Package </h4></div>
                        <div class="">
                            <form class="contact__form v2-search-form" method="post"
                                  action="http://rn53themes.net/themes/demo/travelz/mail/carbooking.php">

                                <div class="row">
                                    <div class="input-field col s12">
                                        <select class="validate">
                                            <option>3 star package</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select class="validate">
                                            <option>3 star package</option>

                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select class="validate">
                                            <option>3 star package</option>

                                        </select>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="submit" value="Compare"
                                               class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 2em;">
                        <div class="hot-page2-hom-pre-head">
                            <h4>Own a travel agency?</h4></div>
                        <div class="">
                            <form class="contact__form v2-search-form" method="post"
                                  action="http://rn53themes.net/themes/demo/travelz/mail/carbooking.php">

                                <div class="row">
                                    <div class="input-field col s12">

                                        <a href="#">List here for free</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 2em;">
                        <div class="hot-page2-hom-pre-head">
                            <h4>Existing Agency Login</h4></div>
                        <div class="">
                            <form class="contact__form v2-search-form" method="post"
                                  action="http://rn53themes.net/themes/demo/travelz/mail/carbooking.php">

                                <div class="row">
                                    <div class="input-field col s12">

                                        <input type="email" class="validate" name="name" required>
                                        <label>Email</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="number" class="validate" name="phone" required>
                                        <label>Password</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="submit" value="Login"
                                               class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
                                        <a href="#">Forgot Password</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




@endsection
