<div class="db-l">

    <div class="db-l-2">
        @if(\Illuminate\Support\Facades\Auth::user()->user_level==1)
            <ul>
                <li>
                    <a href="{{route('dashboard')}}"> Dashboard </a>
                </li>
                <li>
                    <a href="{{route('orderD')}}"> Orders</a>
                </li>
            </ul>
        @elseif(\Illuminate\Support\Facades\Auth::user()->user_level==2)
            <ul>
                <li>
                    <a href="{{route('dashboard')}}"> Dashboard </a>
                </li>
                <li>
                    <a href="{{route('packages.index')}}"> Packages</a>
                </li>
            </ul>

        @elseif(\Illuminate\Support\Facades\Auth::user()->user_level==3)
            <ul>
                <li>
                    <a href="{{route('dashboard')}}"> Dashboard </a>
                </li>
                <li>
                    <a href="{{route('packages.index')}}"> Packages</a>
                </li>
                <li>
                    <a href="{{route('orderD')}}"> Orders</a>
                </li>
            </ul>
        @elseif(\Illuminate\Support\Facades\Auth::user()->user_level==4)
            <ul>
                <li>
                    <a href="{{route('home')}}"> Dashboard </a>
                </li>
                <li>
                    <a href="{{route('packages.index')}}"> Packages</a>
                </li>
                <li>
                    <a href="{{route('hotels.index')}}"> Hotels</a>
                </li>
                <li>
                    <a href="{{route('airports.index')}}">Airport</a>
                </li>
                <li>
                    <a href="{{route('countries.index')}}"> Countries</a>
                </li>
                <li>
                    <a href="{{route('users.index')}}">User</a>
                </li>
                <li>
                    <a href="{{route('pages.index')}}">Pages</a>
                </li>
                <li>
                    <a href="{{route('home')}}">Contact Mails</a>
                </li>
                <li>
                    <a href="{{route('home')}}">Expert Review</a>
                </li>
                <li>
                    <a href="{{route('advertisments.index')}}">Advertisement</a>
                </li>
            </ul>
        @else

        @endif
    </div>
</div>
