<!DOCTYPE html>
<html lang="en" style="overflow-x: hidden;">
<head>
    <title>UROOD</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- FAV ICON -->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:400,500,700" rel="stylesheet">
    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/materialize.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/mob.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">

    <style type="text/css">

        iframe{
            width: 65%;
        }

        @media only screen and (max-width: 320px) {
            .browserr {
                text-align: left;
                width: 124px;
            }
            .main-body{
                text-align: center;
            }
            img{
                max-width: 100%;
                width: 100%;
            }
            .package-review{
                padding: 0 !important;
            }
            .iframe{
                max-width: 100%;
                width: 100%;
            }
            .search-button{
                margin-bottom: 10px;
            }
            .tab-content{
                text-align: justify;
            }
            iframe{
                max-width: 100%;
                width: 100%;
            }
            .ed-mob-menu{
                background-color: #e6b905;
            }

            h1, .h1 {
                font-size: 28px;
            }
            .show{
                font-size: 10px;
            }
        }
        @media only screen and (max-width: 425px){
            .browser {
                min-width: 100%;
                max-width: 100%;
                width: 100%;
            }
            .main-body{
                text-align: center;
            }
            img{
                max-width: 100%;
                width: 100%;
            }
            .package-review{
                padding: 0 !important;
            }
            .iframe{
                max-width: 100%;
                width: 100%;
            }
            .search-button{
                margin-bottom: 10px;
            }
            .tab-content{
                text-align: justify;
            }
            iframe{
                max-width: 100%;
                width: 100%;
            }
            .ed-mob-menu{
                background-color: #e6b905;
            }

        }
        @media only screen and (max-width: 768px){

            .ed-mob-menu{
                background-color: #e6b905;
            }
        }

        @media only screen and (max-width: 375px) {
            .browser {
                min-width: 100%;
                max-width: 100%;
                width: 100%;
            }
            .main-body{
                text-align: center;
            }
            img{
                max-width: 100%;
                width: 100%;
            }
            .package-review{
                padding: 0 !important;
            }
            .iframe{
                max-width: 100%;
                width: 100%;
            }
            .search-button{
                margin-bottom: 10px;
            }
            .tab-content{
                text-align: justify;
            }
            iframe{
                max-width: 100%;
                width: 100%;
            }
            .ed-mob-menu{
                background-color: #e6b905;
            }
        }


    </style>

</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<!-- MOBILE MENU -->
<section>
    <div class="ed-mob-menu">
        <div class="ed-mob-menu-con">
            <div class="ed-mm-left">
                <div class="wed-logo">
                    <a href="main.html"><img src="{{asset('images/logo.png')}}" alt="" style="width: 80px!important; height: 50px!important" />
                    </a>
                </div>
            </div>
            <div class="ed-mm-right">
                <div class="ed-mm-menu">
                    <a href="#!" class="ed-micon"><i class="fa fa-bars"></i></a>
                    <div class="ed-mm-inn">
                        <a href="#!" class="ed-mi-close"><i class="fa fa-times"></i></a>
                        <ul>
                            <li><a href="#">Sign In</a></li>
                            <li><a href="#">Sign Up</a></li>
                            <li><a href="#">Cart</a></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">YouTube</a></li>
                            <li><a href="#">Twitter</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!--HEADER SECTION-->
<section>
    <!-- LOGO AND MENU SECTION -->
    <div class="top-logo" data-spy="affix" data-offset-top="250" style="background-color: #e6b905; padding-top: 5px; padding-bottom: 6px; height: 125px">
        <div class="container">
            <div class="row">
                <div class="col-md-2" style="float: left;">
                    <div class="wed-logo">
                        <a href="{{route('default')}}"><img src="{{asset('images/logo.png')}}" alt=""  style="width: 80px!important; height: 120px!important" />
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="main-menu">
                        <h4>Gateway to the Holy Places</h4>
                        <p>Find Hajj and Umrah packages from independent travel agencies in One Place!</p>
                    </div>
                </div>
                <div class="col-md-4" style="float: right;">
                    <div class="row" style="height: 10px"></div>
                    <div class="wed-logo" style="width: 100%">
                        <div class="ed-com-t1-right" style="width: 100%">
                            <div class="row">
                                <ul>
                                    <li><a href="#" style="background: transparent;">
                                            <i class="fa fa-facebook" aria-hidden="true" style="color: #000;display: inline-block; border-radius: 50%; box-shadow: 0px 0px 2px #888; padding: 0.5em 0.7em;"></i>
                                        </a>
                                    </li>
                                    <li><a href="#" style="background: transparent;"><i class="fa fa-youtube" aria-hidden="true" style="color: #000;display: inline-block; border-radius: 50%; box-shadow: 0px 0px 2px #888; padding: 0.5em 0.7em;"></i></a>
                                    </li>
                                    <li><a href="#" style="background: transparent;"><i class="fa fa-twitter" aria-hidden="true" style="color: #000;display: inline-block; border-radius: 50%; box-shadow: 0px 0px 2px #888; padding: 0.5em 0.7em;"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <ul style="margin-top: 10px">
                                    @if(\Auth::user())
                                        <li><a onclick="event.preventDefault();document.getElementById('logout_form').submit();" ><i class="fa fa-power-off"></i> </a>
                                            <form style="display:none;" id='logout_form' action="{{route('logout')}}" method="post">
                                                @csrf
                                            </form></li>
                                        @else
                                        <li><a href="{{route('login')}}">Sign In</a>
                                        </li>
                                        <li><a href="{{route('register')}}">Sign Up</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="#" style="background: #0a8c8c; background: linear-gradient(to bottom,#0a8c8c,#26a69a);color: #fff; padding: 7px 12px; line-height: 30px; font-size: 12px;">
                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="color: #fff;"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="ed-com-t1-social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                    <div class="row" style="height: 10px"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- TOP SEARCH BOX -->

    <!-- END TOP SEARCH BOX -->
</section>
<!--END HEADER SECTION-->
	<!--====== HOME PLACES ==========-->
	@yield('content')
	<!--====== FOOTER - COPYRIGHT ==========-->
     <section >
        <div class="rows" >
            <div class="footer" style="padding: 20px 0; font-size: 11px;background: linear-gradient(gold 30%, goldenrod 70%);">
                <div class="container">
                    <div class="foot-sec2">
                        <div>
                            <div class="row" style="color: #000">
                                <div class="col-sm-2 col-xs-3 foot-spec foot-com">
                                    <ul style="list-style-type: none;">
                                        <li> <a href="about.html"><p><b>About UROOD</b></p></a> </li>
                                        <li> <a href="contact.html"><p><b>Contact UROOD</b></p></a> </li>
                                        <li> <a href="#"><p><b>Advertise with us</b></p></a> </li>
                                        <li> <a href="#"><p><b>Press Release</b></p></a></li>

                                    </ul>
                                </div>
                                <div class="col-sm-3 col-xs-3 foot-spec foot-com">
                                    <ul style="list-style-type: none;">
                                        <li> <a href="#"><p><b>For Hajj Agencies</b></p></a> </li>
                                        <li> <a href="#"><p><b>Affiliate Program</b></p></a> </li>
                                        <li> <a href="#"><p><b>Site Map</b></p></a> </li>
                                        <li> <a href="#"><p><b>FAQ</b></p></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-2 col-xs-3 foot-spec foot-com">
                                    <ul style="list-style-type: none;">
                                        <li> <a href="#"><p><b>Terms Of use`</b></p></a> </li>
                                        <li> <a href="#"><p><b>Privacy Policy</b></p></a> </li>
                                        <li> <a href="#"><p><b>Advertise with us</b></p></a> </li>
                                        <li> <a href="#"><p><b>Press Release</b></p></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-2 col-xs-3 foot-spec foot-com">
                                    <ul style="list-style-type: none;">
                                        <li> <a href="#"><p><b>Twitter</b></p></a> </li>
                                        <li> <a href="#"><p><b>Google+</b></p></a> </li>
                                        <li> <a href="#"><p><b>Facebook</b></p></a> </li>
                                        <li> <a href="#"><p><b>Youtube</b></p></a></li>
                                    </ul>
                                </div>

                                <div class="col-sm-1 col-xs-12 text-center">
                                    <a href="{{route('default')}}">
                                    <img src="{{asset('images/logo.png')}}" alt="#" class="lgimgk">
                                    </a>
                                </div>
                                <div class="col-sm-2 col-xs-12 text-center">
                                    <br><br>
                                    <h4 style="color: #000">Gateway to the Holy Places</h4>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<section>
		<div class="rows copy">
			<div class="container">
				<p>Copyrights © 2017 Company Name. All Rights Reserved</p>
			</div>
		</div>
	</section>
{{--	<section>--}}
{{--		<div class="icon-float">--}}
{{--			<ul>--}}
{{--				<li><a href="#" class="sh">1k <br> Share</a> </li>--}}
{{--				<li><a href="#" class="fb1"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>--}}
{{--				<li><a href="#" class="gp1"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>--}}
{{--				<li><a href="#" class="tw1"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>--}}
{{--				<li><a href="#" class="li1"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>--}}
{{--				<li><a href="#" class="wa1"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li>--}}
{{--				<li><a href="#" class="sh1"><i class="fa fa-envelope-o" aria-hidden="true"></i></a> </li>--}}
{{--			</ul>--}}
{{--		</div>--}}
{{--	</section>--}}
	<!--========= Scripts ===========-->
<script src="{{asset('js/jquery-latest.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/wow.min.js')}}"></script>
<script src="{{asset('js/materialize.min.js')}}"></script>
<script src="{{asset('js/mail.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );


</script>
    @yield('script')
</body>


</html>
