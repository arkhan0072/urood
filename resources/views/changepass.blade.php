@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
            @include('leftbar')
            <!--CENTER SECTION-->
            <div class="db-2">
                <div class="tr-regi-form" style="width:80%;">
                    <h4>Change Password</h4>

                    <form action="{{route('updatePassword',$user->id)}}" method="post" class="col s12">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <h5 style="text-align:left">New Password</h5>
                                <input type="password" name="password" class="validate">
                            </div>
                        </div>


                        <div class="row">

                            <div class="input-field col s12">
                                <h5 style="text-align:left">Confirm Password</h5>
                                <input type="password" name="confirm_password" class="validate">
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="submit" value="Submit"
                                           class="waves-effect waves-light btn-large full-btn"></div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
@endsection
