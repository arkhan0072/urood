@extends('master')
    @section('content')
        <!--DASHBOARD-->
            <section>
                <div class="db">
                    <!--LEFT SECTION-->
                @include('leftbar')
                <!--CENTER SECTION-->
        <div class="db-2">
            <div class="tr-regi-form" style="width:80%;">
                <h4>Add Advertisement</h4>

                <form method="post" action="{{route('advertisments.store')}}" class="col s12">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <h5 style="text-align:left">Location</h5>
                            <input type="text" name="location" class="validate @error('location') is-invalid @enderror"
                                   value="{{ old('location') }}">
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <h5 style="text-align:left">Content</h5>
                            <textarea name="details" id="editor"></textarea>
                            @error('content')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <h5 style="text-align:left">Active</h5>
                            <select name="status">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                            @error('status')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                    </div>

                    <div class="input-field col s12 checkbox" style="text-align:left">

                        <input id="chp3" class="styled" type="checkbox" style="opacity:1;left:0" name="homePage" value="1">Show in Home Page
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</section>
<!--END DASHBOARD-->
    @endsection
