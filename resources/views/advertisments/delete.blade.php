<a href="#" onclick="frmdlt{{$advertisment->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$advertisment->id}}" action="{{ route('advertisments.destroy', $advertisment->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
