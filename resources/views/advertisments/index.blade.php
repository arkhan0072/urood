@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
           @include('leftbar')
            <!--CENTER SECTION-->
            <div class="db-2" style="padding: 30px;">
                @if(session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif
                <h4 style="text-align:center">Advertisement</h4>
                <div style="float:right;">
                    <a href="{{route('advertisments.create')}}" class="btn btn-primary">Add Advertisment</a>
                </div><br><br>
                <div >
                    <table class="table table-bordered" id="data-table">
                        <tr>
                            <th>Id</th>
                            <th>Location</th>
{{--                            <th>Click Count</th>--}}
                            <th>Active</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
{{--                        @foreach($advertisments as $advertisment)--}}
{{--                        <tr>--}}
{{--                            <td>{{$advertisment->id}}</td>--}}
{{--                            <td>{{$advertisment->location}}</td>--}}
{{--                            <td>11</td>--}}
{{--                            <td>--}}
{{--                                @if($advertisment->status == 1)--}}
{{--                                <i class="fa fa-check" style="color: limegreen;"></i></td>--}}
{{--                                @else--}}
{{--                                <i class="fa fa-close" style="color: red;"></i>--}}
{{--                                @endif--}}
{{--                            </td>--}}
{{--                            <td>{{$advertisment->created_at}}</td>--}}
{{--                            <td>{{$advertisment->updated_at}}</td>--}}
{{--                            <td><i class="fa fa-eye"></i>--}}
{{--                                <a href="{{route('advertisments.edit',$advertisment->id)}}"><i class="fa fa-edit"></i></a>--}}
{{--                                <a href="{{route('advertisments.destory',$advertisment->id)}}"><i class="fa fa-trash-o"></i></a></td>--}}
{{--                        </tr>--}}
{{--                            @endforeach--}}
                    </table>
                </div>


            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
@section('script')
    <script>
        $(function () {

            var table = $('#data-table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('ajaxAdvertisement') }}",
                buttons: [
                    {{--{--}}
                    {{--    text: 'Add Advertisements',--}}
                    {{--    action: '{{route('advertisments.create')}}',--}}
                    {{--    className:'btn btn-primary'--}}
                    {{--},--}}
                    'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id',title:'Id'},
                    {data: 'location', name: 'location',title:'Location'},
                    // {data: 'clickcount', name: 'clickcount',title:'Click Count'},
                    {data: 'status', name: 'status',title:'Active'},
                    {data: 'created_at', name: 'airport_type',title:'Created'},
                    {data: 'updated_at', name: 'airport_type',title:'Modified'},
                    {data: 'action', name: 'action',title:'Action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection

@endsection
