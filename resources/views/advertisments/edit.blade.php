@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2">
                <div class="tr-regi-form" style="width:80%;">
                    <h4>Edit Advertisement</h4>

                    <form method="post" action="{{route('advertisments.update',$advertisment->id)}}" class="col s12">
                        @csrf
                        {{method_field('PATCH')}}
                        <div class="row">
                            <div class="input-field col s12">
                                <h5 style="text-align:left">Location</h5>
                                <input type="text" name="location" class="validate @error('location') is-invalid @enderror"
                                       value="{{ old('location',$advertisment->location) }}">
                                @error('location')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <h5 style="text-align:left">Content</h5>
                                <textarea name="details" id="editor">{{$advertisment->content}}</textarea>
                                @error('content')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <h5 style="text-align:left">Active</h5>
                                <select name="status">
                                    <option value="1" {{$advertisment->status == 1 ? "selected":""}}>Yes</option>
                                    <option value="0" {{$advertisment->status == 0 ? "selected":""}}>No</option>
                                </select>
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <div class="input-field col s12 checkbox" style="text-align:left">

                            <input id="chp3" class="styled" type="checkbox" style="opacity:1;left:0" name="homePage" value="1" {{$advertisment->homePage == 1 ? "checked":""}}>Show in Home Page
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
@endsection
