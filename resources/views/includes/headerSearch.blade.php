<section>
		<div class="v2-hom-search">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-md-4">
					<!-- <div class="v2-ho-se-ri">
						
						<h1>Gateway to the Holy Places</h1>
						<p>Find Hajj and Umrah Packages</p>
						
					</div>	 -->					
					</div>	
					<div class="col-xs-12 col-md-12 col-md-8">
					<div class="" style="padding: 20px; width: 100%; float: left;  background: rgba(0, 0, 0, 0.85); box-shadow: inset 0 1px 0 rgba(255,255,255,.15), 0 1px 2px rgba(0,0,0,.05); border: 1px solid #161616; color: #fff">
                        <h1 style="color: goldenrod;  font-family: 'Open Sans Condensed', sans-serif; font-weight: 700;">Gateway to the Holy Places</h1>
                        <p style="margin-bottom: 15px; font-size: 21px; font-weight: 200;color: #7eb216;">Find Hajj and Umrah Packages</p>

                        <div class="row">
                            <div class="col-xs-7 col-sm-7 col-md-7">
                                <ul class="nav nav-tabs hajjUmrahNav" role="tablist">
                                    <li class="nav-item active"> 
                                      <a class="nav-link active" data-toggle="tab" href="#home" >Hajj</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" data-toggle="tab" href="#menu1">Umrah</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="home" class="container tab-pane active"><br>
                                        <form>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">Makkah First</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">Madina First</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">By Price</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">By Dates</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">Hotels Stars (Ratings)</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">See All</label>
                                            </div>
                                            <div style="border-bottom: .5px solid #fff; width: 20%; margin-top: 5px"></div>
                                          </form>

                                          <form>
                                              <p>Additional Keywords</p>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-3 col-xl-3">
                                                        <input type="text" name="Keywords" placeholder="Keywords" class="form-control" style="width: 100%">
                                                    </div>
                                                </div>
                                              <br>
                                              <button class="btn btn-primary">Search</button>
                                              <button class="btn btn-info">Clear Search</button>
                                          </form>
                                          <br>
                                          <a href="Advanced.html" style="color: goldenrod; font-size: 1.4rem; display: block;float: left;">Advanced Search</a>
                                    </div>
                                    <div id="menu1" class="container tab-pane fade"><br>
                                      <form>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">Makkah First</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">Madina First</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">By Price</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">By Dates</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">Hotels Stars (Ratings)</label>
                                            </div>
                                            <div class="checkbox">
                                              <label style="font-size: 1em;color: #ffffff;"><input type="checkbox" value="">See All</label>
                                            </div>
                                            <div style="border-bottom: .5px solid #fff; width: 20%; margin-top: 5px"></div>
                                          </form>

                                          <form>
                                              <p>Additional Keywords</p>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-3 col-xl-3">
                                                        <input type="text" name="Keywords" placeholder="Keywords" class="form-control" style="width: 100%">
                                                    </div>
                                                </div>
                                              <br>
                                              <button class="btn btn-primary">Search</button>
                                              <button class="btn btn-info">Clear Search</button>
                                          </form>
                                          <br>
                                          <a href="#" style="color: goldenrod; font-size: 1.4rem; display: block;float: left;">Advanced Search</a>
                                            
                                        <br><br>
                                        <div class="row">
                                        <div class="col-xs-12">
                                        <p style="position: relative; bottom: -13px; left: 56px;">By using Urood.com you hereby agree to the <a href="#" style="color: goldenrod; font-size: 1.4rem;" >Terms and Conditions</a> of the website.</p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5" style="color: #7eb216;">
                                <span>Total 0 Packages Found</span> 
                                <a href="#" style="background: #0a8c8c; background: linear-gradient(to bottom,#0a8c8c,#26a69a); color: #fff; padding: 7px 12px; line-height: 30px; font-size: 12px;" style="display: inline-block;">Browse All</a>
                            </div>
                            <br><br>
                            <div class="row">
                            <div class="col-xs-12">
                            <p style="margin-top: 10px">By using Urood.com you hereby agree to the <a href="#" style="color: goldenrod; font-size: 1.4rem;" >Terms and Conditions</a> of the website.</p>
                            </div>
                            </div>
                        </div>
					</div>						
					</div>				
				</div>
			</div>
		</div>
	</section>