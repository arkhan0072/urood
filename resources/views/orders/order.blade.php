@extends('master')
@section('content')

    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
            <div class="db-l">

                <div class="db-l-2">
                    <ul>
                        <li>
                            <a href="dashboard.html"> Dashboard </a>
                        </li>
                        <li>
                            <a href="orders.html"> Orders</a>
                        </li>

                    </ul>
                </div>
            </div>
            <!--CENTER SECTION-->
            <div class="db-2">

                <h4 style="text-align:center">Orders</h4>

                <div style="overflow-x:auto;">
                    <table class="table table-responsive table-bordered" id="data-table">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Package Type</th>
                            <th>Description</th>
                            <th>Depart</th>
                            <th>Return</th>
                            <th>Total Days</th>
                            <th>Makkah Hotel</th>
                            <th>Madinah Hotel</th>
                            <th>Agency</th>
                            <th>Price</th>
                            <th>Package Bought</th>
                            <th>Payment Status</th>
                            <th>Actions</th>
                        </tr>
{{--                        <tr>--}}
{{--                            <td>576</td>--}}
{{--                            <td>Vip Express (Madinah First - No Azizia - Room Access - VIP Tents)</td>--}}
{{--                            <td>Hajj</td>--}}
{{--                            <td>Vip Express (Madinah First - No Azizia - Room Access - VIP Tents)</td>--}}
{{--                            <td>08/04/2019</td>--}}
{{--                            <td>08/15/2019</td>--}}
{{--                            <td>11</td>--}}
{{--                            <td><a href="#">Swissotel Makkah</a></td>--}}
{{--                            <td><a href="#">Dallah Taibah Hotel</a></td>--}}
{{--                            <td><a href="#">Dawn Travels</a></td>--}}
{{--                            <td>4000$</td>--}}
{{--                            <td>4</td>--}}
{{--                            <td>Paid</td>--}}
{{--                            <td>--}}
{{--                                <i class="fa fa-eye"></i>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
                    </table>
                </div>


            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
@section('script')
    <script>
        $(function () {

            var table = $('#data-table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('orderAjax') }}",
                buttons: [
                    {{--{--}}
                        {{--    text: 'Add User',--}}
                        {{--    action: '{{route('users.create')}}',--}}
                        {{--    className:'btn btn-primary'--}}
                        {{--},--}}
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id', title: 'id'},
                    {data: 'country.name', name: 'name', title: 'Country'},
                    {data: 'fname', name: 'fname', title: 'First Name'},
                    {data: 'lname', name: 'lname', title: 'Last Name'},
                    {data: 'email', name: 'email', title: 'Email'},
                    {data: 'address', name: 'address', title: 'Address'},
                    {data: 'city', name: 'city', title: 'City'},
                    {data: 'state', name: 'state', title: 'State'},
                    {data: 'zipcode', name: 'zipcode', title: 'ZipCode'},
                    {data: 'action', name: 'action', title: 'Action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    @endsection
@endsection
