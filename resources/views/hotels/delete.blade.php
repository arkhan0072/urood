<a href="#" onclick="frmdlt{{$hotel->id}}.submit();"  title="Delete"><i class="fa fa-trash"></i> </a>
<form name="frmdlt{{$hotel->id}}" action="{{ route('hotels.destroy', $hotel->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
