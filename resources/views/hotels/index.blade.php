@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
            <div class="db-2" style="padding: 30px;">
                @if(session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif
                <h4 style="text-align:center">Hotels</h4>

                <div style="float:right;">
                    <a href="{{route('hotels.create')}}" class="btn btn-primary">Add Hotel</a>
                </div>
                <br><br>
                <div style="overflow-x:auto;">
                    <table class="table table-bordered" id="data-table" style="width: 100%;">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>City</th>
                            <th>Star</th>
                            <th>Actions</th>
                        </tr>
                        {{--                    @foreach($hotels as $hotel)--}}
                        {{--                    <tr>--}}
                        {{--                        <td>{{$hotel->id}}</td>--}}
                        {{--                        <td>{{$hotel->name}}</td>--}}
                        {{--                        <td>{{$hotel->city}}</td>--}}
                        {{--                        <td>{{$hotel->star}}</td>--}}
                        {{--                        <td><i class="fa fa-eye"></i>--}}
                        {{--                            <i class="fa fa-edit"></i>--}}
                        {{--                            <i class="fa fa-trash-o"></i>--}}
                        {{--                            <i class="fa fa-clone"></i></td>--}}
                        {{--                    </tr>--}}
                        {{--                        @endforeach--}}
                    </table>
                </div>


            </div>

        </div>
    </section>
@section('script')
    <script>
        $(function () {

            var table = $('#data-table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                ajax: "{{ route('ajaxHotel') }}",
                buttons: [
                    {{--{--}}
                        {{--    text: 'Add Hotel',--}}
                        {{--    action: '{{route('hotels.create')}}',--}}
                        {{--    className:'btn btn-primary'--}}
                        {{--    },--}}
                        'copy', 'excel', 'pdf'
                ],
                columns: [
                    {data: 'id', name: 'id', title: 'id'},
                    {data: 'name', name: 'name', title: 'Name'},
                    {data: 'city', name: 'city', title: 'City'},
                    {data: 'star', name: 'star', title: 'Star'},
                    {data: 'action', name: 'action', title: 'Action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
<!--END DASHBOARD-->
@endsection
