@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
        @include('leftbar')
        <!--CENTER SECTION-->
			<div class="db-2">
				<div class="tr-regi-form" style="width:80%;">
				<h4>Add Hotel</h4>

				<form method="post" action="{{route('hotels.store')}}" class="col s12" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<div class="input-field col m6 s12">
						<h5 style="text-align:left">Name</h5>
							<input type="text" name="name" class="validate @error('name') is-invalid @enderror"
							value="{{ old('name') }}">
							  @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div class="input-field col m4 s12">
						<h5 style="text-align:left" >City</h5>
							<select name="city" class="validate @error('city') is-invalid @enderror">

							<option value="Makkah">Makkah</option>
							<option value="Madinah">Madinah</option>

							</select>
							  @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div class="input-field col m2 s12">
						<h5 style="text-align:left">Star</h5>
							<select name="star" class="validate @error('star') is-invalid @enderror">

							<option value="3">1</option>
							<option value="4">2</option>
							<option value="5">3</option>
							<option value="6">4</option>
							<option value="7">5</option>
							</select>
							 @error('star')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
					</div>



					<div class="row">
						<div class="input-field col s12">
						<h5 style="text-align:left">Description</h5>
							<textarea name="description" id="editor"></textarea>
							@error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
					</div>



					<div class="row showdata">
					<h5 style="text-align:left">Upload Photo</h5>
						<div class="data_append">
						<div class="input-field col m4 s12">
							<input type="text" name="pictures[]" class="validate">
						</div>

						<div class="input-field col m6 s12">

							 <input type="file" name="pic[]" accept="image/*">

						</div>

						<div class="input-field col m2 s12">

							<button class="btn btn-primary addnew">&#43;</button>
							<button class="btn btn-primary remove">-</button>
						</div>
					</div>
</div>

					<div class="row">
						<div class="input-field col s12">
							<input type="submit" value="Save" class="waves-effect waves-light btn-large full-btn"> </div>
					</div>

				</form>

			</div>
			</div>

		</div>
	</section>
	<!--END DASHBOARD-->

	@section("script")
	<script type="text/javascript">

		$('.addnew').click(function(e){
		e.preventDefault();
			$('.showdata').append('<div class="data_append"><div class="input-field col m4 s12"><input type="text" name="pictures[]" class="validate"></div><div class="input-field col m6 s12"><input type="file" name="pic[]" accept="image/*"></div><div class="input-field col m2 s12"><button class="btn btn-primary addnew">&#43;</button><button class="btn btn-primary remove">-</button>  </div></div>');

		});
		$('.remove').click(function(e){
		e.preventDefault();
			$(this).closest('.data_append').remove();

});
	</script>
	@endsection
	@stop
