@extends('master')
@section('content')
    <!--DASHBOARD-->
    <section>
        <div class="db">
            <!--LEFT SECTION-->
            @include('leftbar')
            <!--CENTER SECTION-->
            <div class="db-2">
                <div class="db-2-com db-2-main">

                    <div class="db-2-main-com">
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db2.png')}}" alt="" />
                                <a href="{{route('packages.index')}}"><span>Packages</span></a>

                            </div>
                        </div>
                        <div class="db-2-main-1">
                            <div class="db-2-main-2"> <img src="{{asset('images/icon/db10.png')}}" alt="" />
                                <a href="{{route('changepassword')}}"><span>Change Password</span></a>

                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </section>
    <!--END DASHBOARD-->
    @endsection
